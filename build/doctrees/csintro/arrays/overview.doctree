���=      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Introduction�h]�h �Text����Introduction�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�vC:\Users\nelve\Documents\bitbucket\microbit-introduction-to-block-based-programming\source\csintro\arrays\overview.rst�hKubh �	paragraph���)��}�(h��Any collector of coins, fossils, or baseball cards knows that at some
point you need to have a way to organize everything so you can find
things. For example, a rock collector might have a tray of specimens
numbered like this:�h]�h��Any collector of coins, fossils, or baseball cards knows that at some
point you need to have a way to organize everything so you can find
things. For example, a rock collector might have a tray of specimens
numbered like this:�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �figure���)��}�(hhh]�(h �image���)��}�(h��.. figure:: /static/courses/csintro/arrays/rock-collection.png
   :alt: Rock collection is an array

   Rock collection is an array
�h]�h}�(h]�h!]�h#]�h%]�h']��alt��Rock collection is an array��uri��1static/courses/csintro/arrays/rock-collection.png��
candidates�}��*�hOsuh)h@hh=hh*hKubh �caption���)��}�(h�Rock collection is an array�h]�h�Rock collection is an array�����}�(hhWhhUubah}�(h]�h!]�h#]�h%]�h']�uh)hShh*hKhh=ubeh}�(h]��id1�ah!]�h#]�h%]�h']�uh)h;hKhhhhhh*ubh,)��}�(h�hEvery rock in the collection needs its own storage space, and a unique
address so you can find it later.�h]�h�hEvery rock in the collection needs its own storage space, and a unique
address so you can find it later.�����}�(hhlhhjhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh,)��}�(h��As your MakeCode programs get more and more complicated, and require
more variables to keep track of things, you will want to find a way to
store and organize all of your data. MakeCode provides a special
category for just this purpose, called an Array.�h]�h��As your MakeCode programs get more and more complicated, and require
more variables to keep track of things, you will want to find a way to
store and organize all of your data. MakeCode provides a special
category for just this purpose, called an Array.�����}�(hhzhhxhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�YArrays can store numbers, strings (words), or sprites. They can also
store musical notes.�h]�h,)��}�(h�YArrays can store numbers, strings (words), or sprites. They can also
store musical notes.�h]�h�YArrays can store numbers, strings (words), or sprites. They can also
store musical notes.�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhhh*hNubh�)��}�(h��Every spot in an array can be identified by its index, which is a
number that corresponds to its location in the array. The first slot
in an array is index 0, just like our rock collection above.�h]�h,)��}�(h��Every spot in an array can be identified by its index, which is a
number that corresponds to its location in the array. The first slot
in an array is index 0, just like our rock collection above.�h]�h��Every spot in an array can be identified by its index, which is a
number that corresponds to its location in the array. The first slot
in an array is index 0, just like our rock collection above.�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhhh*hNubh�)��}�(h��The length of an array refers to the total number of items in the
array, and the index of the last element in an array is always one
less than its length (because the array numbering starts at zero.)
�h]�h,)��}�(h��The length of an array refers to the total number of items in the
array, and the index of the last element in an array is always one
less than its length (because the array numbering starts at zero.)�h]�h��The length of an array refers to the total number of items in the
array, and the index of the last element in an array is always one
less than its length (because the array numbering starts at zero.)�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)h�hh*hKhhhhubh,)��}�(h��In MakeCode, you can create an array by assigning it to a variable. The
Array blocks can be found under the Advanced Toolbox menu.�h]�h��In MakeCode, you can create an array by assigning it to a variable. The
Array blocks can be found under the Advanced Toolbox menu.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh<)��}�(hhh]�(hA)��}�(h�l.. figure:: /static/courses/csintro/arrays/arrays-menu.png
   :alt: Arrays block menu

   Arrays block menu
�h]�h}�(h]�h!]�h#]�h%]�h']��alt��Arrays block menu��uri��-static/courses/csintro/arrays/arrays-menu.png�hP}�hRh�suh)h@hh�hh*hK%ubhT)��}�(h�Arrays block menu�h]�h�Arrays block menu�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hShh*hK%hh�ubeh}�(h]��id2�ah!]�h#]�h%]�h']�uh)h;hK%hhhhhh*ubh,)��}�(h��The code above creates an empty array called list, then fills it with
five numbers, indexed from 0 to 4. The index of the first value (4) is
0. The index of the second value (2) is 1. The index of the last value
(3) is 4.�h]�h��The code above creates an empty array called list, then fills it with
five numbers, indexed from 0 to 4. The index of the first value (4) is
0. The index of the second value (2) is 1. The index of the last value
(3) is 4.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK+hhhhubh,)��}�(h�EYou can get items out of the array by specifying its index like this:�h]�h�EYou can get items out of the array by specifying its index like this:�����}�(hj"  hj   hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK0hhhhubh,)��}�(h�fThe code above takes the first element in the array (the value at index
0) and shows it on the screen.�h]�h�fThe code above takes the first element in the array (the value at index
0) and shows it on the screen.�����}�(hj0  hj.  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK:hhhhubh,)��}�(h�pThere are lots of other blocks in the Arrays Toolbox drawer. The next
few Activities will introduce you to them.�h]�h�pThere are lots of other blocks in the Arrays Toolbox drawer. The next
few Activities will introduce you to them.�����}�(hj>  hj<  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK=hhhhubh
)��}�(hhh]�(h)��}�(h�
Discussion�h]�h�
Discussion�����}�(hjO  hjM  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjJ  hhhh*hKAubh�)��}�(hhh]�(h�)��}�(h�nAsk your students if any of them collects anything. What is it? Comic
books, cards, coins, stamps, books, etc.�h]�h,)��}�(h�nAsk your students if any of them collects anything. What is it? Comic
books, cards, coins, stamps, books, etc.�h]�h�nAsk your students if any of them collects anything. What is it? Comic
books, cards, coins, stamps, books, etc.�����}�(hjd  hjb  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKChj^  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj[  hhhh*hNubh�)��}�(h�How big is the collection?�h]�h,)��}�(hjx  h]�h�How big is the collection?�����}�(hjx  hjz  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKEhjv  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj[  hhhh*hNubh�)��}�(h�How is it organized?�h]�h,)��}�(hj�  h]�h�How is it organized?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKFhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj[  hhhh*hNubh�)��}�(h� Are the items sorted in any way?�h]�h,)��}�(hj�  h]�h� Are the items sorted in any way?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKGhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj[  hhhh*hNubh�)��}�(h�DHow would you go about finding a particular item in the collection?
�h]�h,)��}�(h�CHow would you go about finding a particular item in the collection?�h]�h�CHow would you go about finding a particular item in the collection?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKHhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj[  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh*hKChjJ  hhubh,)��}�(hX�  In the discussion, see if you can explore the following array vocabulary
words in the context of kids’ personal collections. \* Length: the total
number of items in the collection \* Sort: Items in the collection are
ordered by a particular attribute (e.g., date, price, name) \* Index: A
unique address or location in the collection \* Type: The type of item
being stored in the collection�h]�hX�  In the discussion, see if you can explore the following array vocabulary
words in the context of kids’ personal collections. * Length: the total
number of items in the collection * Sort: Items in the collection are
ordered by a particular attribute (e.g., date, price, name) * Index: A
unique address or location in the collection * Type: The type of item
being stored in the collection�����}�(hX�  In the discussion, see if you can explore the following array vocabulary
words in the context of kids’ personal collections. \* Length: the total
number of items in the collection \* Sort: Items in the collection are
ordered by a particular attribute (e.g., date, price, name) \* Index: A
unique address or location in the collection \* Type: The type of item
being stored in the collection�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKJhjJ  hhubeh}�(h]��
discussion�ah!]�h#]��
discussion�ah%]�h']�uh)h	hhhhhh*hKAubh
)��}�(hhh]�(h)��}�(h�
References�h]�h�
References�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKRubh,)��}�(hX�  Once you start saving lots of different values in an array, you will
probably want to have some way to sort those values. Many languages
already implement a sorting algorithm that students can call upon as
needed. However, understanding how those different sorting algorithms
work is an important part of computer science, and as students go on to
further study they will learn other algorithms, as well as their
relative efficiency.�h]�hX�  Once you start saving lots of different values in an array, you will
probably want to have some way to sort those values. Many languages
already implement a sorting algorithm that students can call upon as
needed. However, understanding how those different sorting algorithms
work is an important part of computer science, and as students go on to
further study they will learn other algorithms, as well as their
relative efficiency.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKThj�  hhubh,)��}�(hX`  There are some good array sorting videos: \* Visually displays a number
of different types of sorts: https://www.youtube.com/watch?v=kPRA0W1kECg
\* Bubble-sort with Hungarian folk dance: https://youtu.be/lyZQPjUT5B4
\* Select-sort with Gypsy folk dance: https://youtu.be/Ns4TPTC8whw \*
Insert-sort with Romanian folk dance: https://youtu.be/ROalU379l3U�h]�(h�dThere are some good array sorting videos: * Visually displays a number
of different types of sorts: �����}�(h�eThere are some good array sorting videos: \* Visually displays a number
of different types of sorts: �hj  hhhNhNubh �	reference���)��}�(h�+https://www.youtube.com/watch?v=kPRA0W1kECg�h]�h�+https://www.youtube.com/watch?v=kPRA0W1kECg�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']��refuri�j  uh)j  hj  ubh�*
* Bubble-sort with Hungarian folk dance: �����}�(h�+
\* Bubble-sort with Hungarian folk dance: �hj  hhhNhNubj  )��}�(h�https://youtu.be/lyZQPjUT5B4�h]�h�https://youtu.be/lyZQPjUT5B4�����}�(hhhj.  ubah}�(h]�h!]�h#]�h%]�h']��refuri�j0  uh)j  hj  ubh�&
* Select-sort with Gypsy folk dance: �����}�(h�'
\* Select-sort with Gypsy folk dance: �hj  hhhNhNubj  )��}�(h�https://youtu.be/Ns4TPTC8whw�h]�h�https://youtu.be/Ns4TPTC8whw�����}�(hhhjB  ubah}�(h]�h!]�h#]�h%]�h']��refuri�jD  uh)j  hj  ubh�) *
Insert-sort with Romanian folk dance: �����}�(h�* \*
Insert-sort with Romanian folk dance: �hj  hhhNhNubj  )��}�(h�https://youtu.be/ROalU379l3U�h]�h�https://youtu.be/ROalU379l3U�����}�(hhhjV  ubah}�(h]�h!]�h#]�h%]�h']��refuri�jX  uh)j  hj  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK\hj�  hhubeh}�(h]��
references�ah!]�h#]��
references�ah%]�h']�uh)h	hhhhhh*hKRubeh}�(h]��introduction�ah!]�h#]��introduction�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jx  ju  j�  j�  jp  jm  u�	nametypes�}�(jx  Nj�  Njp  Nuh}�(ju  hj�  jJ  jm  j�  heh=j  h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�(h,)��}�(h�:Cannot analyze code. No Pygments lexer found for "blocks".�h]�h�>Cannot analyze code. No Pygments lexer found for “blocks”.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubh �literal_block���)��}�(h�0.. code:: blocks

   let list = [4, 2, 5, 1, 3]
�h]�h�0.. code:: blocks

   let list = [4, 2, 5, 1, 3]
�����}�(hhhj
  ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)j  hj�  hh*ubeh}�(h]�h!]�h#]�h%]�h']��level�K�type��WARNING��line�K'�source�h*uh)j�  hhhhhh*hK*ubj�  )��}�(hhh]�(h,)��}�(h�:Cannot analyze code. No Pygments lexer found for "blocks".�h]�h�>Cannot analyze code. No Pygments lexer found for “blocks”.�����}�(hhhj(  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj%  ubj	  )��}�(h��.. code:: blocks

   let list = [4, 2, 5, 1, 3]

   input.onButtonPressed(Button.A, () => {
       basic.showNumber(list[0])
   })
�h]�h��.. code:: blocks

   let list = [4, 2, 5, 1, 3]

   input.onButtonPressed(Button.A, () => {
       basic.showNumber(list[0])
   })
�����}�(hhhj6  ubah}�(h]�h!]�h#]�h%]�h']�j  j  uh)j  hj%  hh*ubeh}�(h]�h!]�h#]�h%]�h']��level�K�type�j"  �line�K2�source�h*uh)j�  hhhhhh*hK9ube�transform_messages�]��transformer�N�
decoration�Nhhub.