��e�      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Project: Everything counts�h]�h �Text����Project: Everything counts�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�xC:\Users\nelve\Documents\bitbucket\microbit-introduction-to-block-based-programming\source\csintro\variables\project.rst�hKubh �	paragraph���)��}�(hX=  This is an assignment for students to come up with a micro:bit program
that counts something. Their program should keep track of **input** by
storing values in variables, and provide **output** in some visual and
useful way. Students should also perform mathematical operations on the
variables to give useful output.�h]�(h��This is an assignment for students to come up with a micro:bit program
that counts something. Their program should keep track of �����}�(h��This is an assignment for students to come up with a micro:bit program
that counts something. Their program should keep track of �hh-hhhNhNubh �strong���)��}�(h�	**input**�h]�h�input�����}�(hhhh8ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hh-ubh�- by
storing values in variables, and provide �����}�(h�- by
storing values in variables, and provide �hh-hhhNhNubh7)��}�(h�
**output**�h]�h�output�����}�(hhhhKubah}�(h]�h!]�h#]�h%]�h']�uh)h6hh-ubh�| in some visual and
useful way. Students should also perform mathematical operations on the
variables to give useful output.�����}�(h�| in some visual and
useful way. Students should also perform mathematical operations on the
variables to give useful output.�hh-hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh
)��}�(hhh]�(h)��}�(h�Input�h]�h�Input�����}�(hhihhghhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhhdhhhh*hKubh,)��}�(h�XRemind the students of all the different inputs available to them
through the micro:bit.�h]�h�XRemind the students of all the different inputs available to them
through the micro:bit.�����}�(hhwhhuhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhdhhubh �figure���)��}�(hhh]�(h �image���)��}�(h�t.. figure:: /static/courses/csintro/variables/input-list.png
   :alt: micro:bit input list

   micro:bit input list
�h]�h}�(h]�h!]�h#]�h%]�h']��alt��micro:bit input list��uri��/static/courses/csintro/variables/input-list.png��
candidates�}��*�h�suh)h�hh�hh*hKubh �caption���)��}�(h�micro:bit input list�h]�h�micro:bit input list�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hKhh�ubeh}�(h]��id1�ah!]�h#]�h%]�h']�uh)h�hKhhdhhhh*ubeh}�(h]��input�ah!]�h#]��input�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Project Ideas�h]�h�Project Ideas�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hKubh
)��}�(hhh]�(h)��}�(h�Duct tape wallet�h]�h�Duct tape wallet�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hKubh,)��}�(hX  You can see the instructions for creating a durable, fashionable wallet
or purse out of duct tape: `Duct tape wallet </projects/wallet>`__.
Create a place for the micro:bit to fit securely. Use Button A to add
dollars to the wallet, and Button B to subtract dollars from the wallet.�h]�(h�cYou can see the instructions for creating a durable, fashionable wallet
or purse out of duct tape: �����}�(h�cYou can see the instructions for creating a durable, fashionable wallet
or purse out of duct tape: �hh�hhhNhNubh �	reference���)��}�(h�'`Duct tape wallet </projects/wallet>`__�h]�h�Duct tape wallet�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��name��Duct tape wallet��refuri��/projects/wallet�uh)h�hh�ubh��.
Create a place for the micro:bit to fit securely. Use Button A to add
dollars to the wallet, and Button B to subtract dollars from the wallet.�����}�(h��.
Create a place for the micro:bit to fit securely. Use Button A to add
dollars to the wallet, and Button B to subtract dollars from the wallet.�hh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�hhubh,)��}�(h��**Extra mod:** Use other inputs to handle cents, and provide a way to
display how much money is in the wallet in dollars and cents.�h]�(h7)��}�(h�**Extra mod:**�h]�h�
Extra mod:�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj  ubh�u Use other inputs to handle cents, and provide a way to
display how much money is in the wallet in dollars and cents.�����}�(h�u Use other inputs to handle cents, and provide a way to
display how much money is in the wallet in dollars and cents.�hj  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK hh�hhubeh}�(h]��duct-tape-wallet�ah!]�h#]��duct tape wallet�ah%]�h']�uh)h	hh�hhhh*hKubh
)��}�(hhh]�(h)��}�(h�1Umpire’s baseball counter (pitches and strikes)�h]�h�1Umpire’s baseball counter (pitches and strikes)�����}�(hj.  hj,  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj)  hhhh*hK$ubh,)��}�(h��In baseball during an at-bat, umpires must keep track of how many
pitches have been thrown to each batter. Use Button A to record the
number of balls (up to 4) and the number of strikes (up to 3).�h]�h��In baseball during an at-bat, umpires must keep track of how many
pitches have been thrown to each batter. Use Button A to record the
number of balls (up to 4) and the number of strikes (up to 3).�����}�(hj<  hj:  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK&hj)  hhubh,)��}�(h��**Extra mod:** Create a way to reset both variables to zero, create a
way to see the number of balls and strikes on the screen at the same
time.�h]�(h7)��}�(h�**Extra mod:**�h]�h�
Extra mod:�����}�(hhhjL  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hjH  ubh�� Create a way to reset both variables to zero, create a
way to see the number of balls and strikes on the screen at the same
time.�����}�(h�� Create a way to reset both variables to zero, create a
way to see the number of balls and strikes on the screen at the same
time.�hjH  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK*hj)  hhubeh}�(h]��,umpires-baseball-counter-pitches-and-strikes�ah!]�h#]��1umpire’s baseball counter (pitches and strikes)�ah%]�h']�uh)h	hh�hhhh*hK$ubh
)��}�(hhh]�(h)��}�(h�Shake counter�h]�h�Shake counter�����}�(hjr  hjp  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjm  hhhh*hK/ubh,)��}�(hX  Using the ‘On Shake’ block, you can detect when the micro:bit has been
shaken and increment a variable accordingly. Try attaching the micro:bit
to a lacrosse stick and keep track of how many times you have
successfully thrown the ball up in the air and caught it.�h]�hX  Using the ‘On Shake’ block, you can detect when the micro:bit has been
shaken and increment a variable accordingly. Try attaching the micro:bit
to a lacrosse stick and keep track of how many times you have
successfully thrown the ball up in the air and caught it.�����}�(hj�  hj~  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK1hjm  hhubh,)��}�(h�p**Extra mod:** Make the micro:bit create a sound of increasing pitch
every time you successfully catch the ball.�h]�(h7)��}�(h�**Extra mod:**�h]�h�
Extra mod:�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�b Make the micro:bit create a sound of increasing pitch
every time you successfully catch the ball.�����}�(h�b Make the micro:bit create a sound of increasing pitch
every time you successfully catch the ball.�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK6hjm  hhubeh}�(h]��shake-counter�ah!]�h#]��shake counter�ah%]�h']�uh)h	hh�hhhh*hK/ubh
)��}�(hhh]�(h)��}�(h�	Pedometer�h]�h�	Pedometer�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK:ubh,)��}�(h��See if you can count your steps while running or doing other physical
activities carrying the micro:bit. Where is it best mounted?�h]�h��See if you can count your steps while running or doing other physical
activities carrying the micro:bit. Where is it best mounted?�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK<hj�  hhubh,)��}�(h�**Extra Mod:** Design a wearable band or holder that can carry the
micro:bit securely so it doesn’t slip out during exercise.�h]�(h7)��}�(h�**Extra Mod:**�h]�h�
Extra Mod:�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�q Design a wearable band or holder that can carry the
micro:bit securely so it doesn’t slip out during exercise.�����}�(h�q Design a wearable band or holder that can carry the
micro:bit securely so it doesn’t slip out during exercise.�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK?hj�  hhubeh}�(h]��	pedometer�ah!]�h#]��	pedometer�ah%]�h']�uh)h	hh�hhhh*hK:ubh
)��}�(hhh]�(h)��}�(h�
Calculator�h]�h�
Calculator�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKCubh,)��}�(h��Create an adding machine. Use Button A to increment the first number,
and Button B to increment the second number. Then, use Shake or Buttons
A + B to add the two numbers and display their sum.�h]�h��Create an adding machine. Use Button A to increment the first number,
and Button B to increment the second number. Then, use Shake or Buttons
A + B to add the two numbers and display their sum.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKEhj�  hhubh,)��}�(h�F**Extra mod:** Find a way to select and perform other math operations.�h]�(h7)��}�(h�**Extra mod:**�h]�h�
Extra mod:�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj  ubh�8 Find a way to select and perform other math operations.�����}�(h�8 Find a way to select and perform other math operations.�hj  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKIhj�  hhubh,)��}�(h�O|micro:bit top and spin counter| Homemade top with micro:bit revolution
counter�h]�(h�)��}�(h�>image:: /static/courses/csintro/variables/microbit-spinner.png�h]�h}�(h]�h!]�h#]�h%]�h']��alt��micro:bit top and spin counter��uri��5static/courses/csintro/variables/microbit-spinner.png�h�}�h�jB  suh)h�hh*hK�hj1  hhubh�/ Homemade top with micro:bit revolution
counter�����}�(h�/ Homemade top with micro:bit revolution
counter�hj1  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKKhj�  hhubh,)��}�(h�:|Duct tape wallet| Duct tape wallet with micro:bit display�h]�(h�)��}�(h�>image:: /static/courses/csintro/variables/duct-tape-wallet.jpg�h]�h}�(h]�h!]�h#]�h%]�h']��alt��Duct tape wallet��uri��5static/courses/csintro/variables/duct-tape-wallet.jpg�h�}�h�j`  suh)h�hh*hK�hjO  hhubh�( Duct tape wallet with micro:bit display�����}�(h�( Duct tape wallet with micro:bit display�hjO  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKNhj�  hhubh,)��}�(h�/|Baseball pitch counter| Baseball pitch counter�h]�(h�)��}�(h�>image:: /static/courses/csintro/variables/baseball-counter.jpg�h]�h}�(h]�h!]�h#]�h%]�h']��alt��Baseball pitch counter��uri��5static/courses/csintro/variables/baseball-counter.jpg�h�}�h�j~  suh)h�hh*hK�hjm  hhubh� Baseball pitch counter�����}�(h� Baseball pitch counter�hjm  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKPhj�  hhubeh}�(h]��
calculator�ah!]�h#]��
calculator�ah%]�h']�uh)h	hh�hhhh*hKCubeh}�(h]��project-ideas�ah!]�h#]��project ideas�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Process�h]�h�Process�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKSubh,)��}�(hX�  In any design project, it’s important to start by understanding the
problem. You can begin this activity by interviewing people around you
who might have encountered the problem you are trying to solve. For
example, if you are designing a wallet, ask your friends how they store
their money, credit cards, identification, etc. What are some challenges
with their current system? What do they like about it? What else do they
use their wallets for?�h]�hX�  In any design project, it’s important to start by understanding the
problem. You can begin this activity by interviewing people around you
who might have encountered the problem you are trying to solve. For
example, if you are designing a wallet, ask your friends how they store
their money, credit cards, identification, etc. What are some challenges
with their current system? What do they like about it? What else do they
use their wallets for?�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKUhj�  hhubh,)��}�(h��If you are designing something else, think about how you might find out
more information about your problem through interviewing or observing
people using current solutions.�h]�h��If you are designing something else, think about how you might find out
more information about your problem through interviewing or observing
people using current solutions.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK]hj�  hhubh,)��}�(hX�  Then start brainstorming. Sketch out a variety of different ideas.
Remember that it’s okay if the ideas seem far-out or impractical. Some
of the best products come out of seemingly crazy ideas that can
ultimately be worked into the design of something useful. What kind of
holder can you design to hold the micro:bit securely? How will it be
used in the real world, as part of a physical design?�h]�hX�  Then start brainstorming. Sketch out a variety of different ideas.
Remember that it’s okay if the ideas seem far-out or impractical. Some
of the best products come out of seemingly crazy ideas that can
ultimately be worked into the design of something useful. What kind of
holder can you design to hold the micro:bit securely? How will it be
used in the real world, as part of a physical design?�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKahj�  hhubh,)��}�(hX�  Use the simulator to do your programming, and test out a number of
different ideas. What is the easiest way to keep track of data? If you
are designing for the accelerometer, try to see what different values
are generated through different actions (you can display the value the
accelerometer is currently reading using the ‘Show Number’ block; clear
the screen afterward so you can see the reading).�h]�hX�  Use the simulator to do your programming, and test out a number of
different ideas. What is the easiest way to keep track of data? If you
are designing for the accelerometer, try to see what different values
are generated through different actions (you can display the value the
accelerometer is currently reading using the ‘Show Number’ block; clear
the screen afterward so you can see the reading).�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhj�  hhubeh}�(h]��process�ah!]�h#]��process�ah%]�h']�uh)h	hhhhhh*hKSubh
)��}�(hhh]�(h)��}�(h�
Reflection�h]�h�
Reflection�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK}ubh,)��}�(h�[Have students write a reflection of about 150–300 words, addressing the
following points:�h]�h�[Have students write a reflection of about 150–300 words, addressing the
following points:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhj�  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�@What was the problem you were trying to solve with this project?�h]�h,)��}�(hj  h]�h�@What was the problem you were trying to solve with this project?�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhh*hNubj  )��}�(h�CWhat were the Variables that you used to keep track of information?�h]�h,)��}�(hj+  h]�h�CWhat were the Variables that you used to keep track of information?�����}�(hj+  hj-  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj)  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhh*hNubj  )��}�(h�aWhat mathematical operations did you perform on your variables? What
information did you provide?�h]�h,)��}�(h�aWhat mathematical operations did you perform on your variables? What
information did you provide?�h]�h�aWhat mathematical operations did you perform on your variables? What
information did you provide?�����}�(hjF  hjD  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj@  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhh*hNubj  )��}�(h�pDescribe what the physical component of your micro:bit project was
(e.g., an armband, a wallet, a holder, etc.).�h]�h,)��}�(h�pDescribe what the physical component of your micro:bit project was
(e.g., an armband, a wallet, a holder, etc.).�h]�h�pDescribe what the physical component of your micro:bit project was
(e.g., an armband, a wallet, a holder, etc.).�����}�(hj^  hj\  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjX  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhh*hNubj  )��}�(h�RHow well did your prototype work? What were you happy with? What
would you change?�h]�h,)��}�(h�RHow well did your prototype work? What were you happy with? What
would you change?�h]�h�RHow well did your prototype work? What were you happy with? What
would you change?�����}�(hjv  hjt  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjp  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhh*hNubj  )��}�(h�YWhat was something that was surprising to you about the process of
creating this project?�h]�h,)��}�(h�YWhat was something that was surprising to you about the process of
creating this project?�h]�h�YWhat was something that was surprising to you about the process of
creating this project?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhh*hNubj  )��}�(h�fDescribe a difficult point in the process of designing this project,
and explain how you resolved it.
�h]�h,)��}�(h�eDescribe a difficult point in the process of designing this project,
and explain how you resolved it.�h]�h�eDescribe a difficult point in the process of designing this project,
and explain how you resolved it.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)j  hh*hK�hj�  hhubeh}�(h]��
reflection�ah!]�h#]��
reflection�ah%]�h']�uh)h	hhhhhh*hK}ubh
)��}�(hhh]�(h)��}�(h�
Assessment�h]�h�
Assessment�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubh,)��}�(h�!**Competency scores**: 4, 3, 2, 1�h]�(h7)��}�(h�**Competency scores**�h]�h�Competency scores�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�: 4, 3, 2, 1�����}�(h�: 4, 3, 2, 1�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh
)��}�(hhh]�(h)��}�(h�	Variables�h]�h�	Variables�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubh �block_quote���)��}�(hhh]�h,)��}�(h��**4 =** At least 3 different variables are implemented in a
meaningful way. **3 =** At least 2 variables are implemented in a
meaningful way. **2 =** At least 1 variable is implemented in a
meaningful way. **1 =** No variables are implemented.�h]�(h7)��}�(h�**4 =**�h]�h�4 =�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj  ubh�E At least 3 different variables are implemented in a
meaningful way. �����}�(h�E At least 3 different variables are implemented in a
meaningful way. �hj  ubh7)��}�(h�**3 =**�h]�h�3 =�����}�(hhhj#  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj  ubh�; At least 2 variables are implemented in a
meaningful way. �����}�(h�; At least 2 variables are implemented in a
meaningful way. �hj  ubh7)��}�(h�**2 =**�h]�h�2 =�����}�(hhhj6  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj  ubh�9 At least 1 variable is implemented in a
meaningful way. �����}�(h�9 At least 1 variable is implemented in a
meaningful way. �hj  ubh7)��}�(h�**1 =**�h]�h�1 =�����}�(hhhjI  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj  ubh� No variables are implemented.�����}�(h� No variables are implemented.�hj  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj	  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj�  hhhh*hNubeh}�(h]��	variables�ah!]�h#]��	variables�ah%]�h']�uh)h	hj�  hhhh*hK�ubh
)��}�(hhh]�(h)��}�(h�Variable names�h]�h�Variable names�����}�(hju  hjs  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjp  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(hX�  **4 =** All variable names are unique and clearly describe what
information values the variables hold. **3 =** The majority of
variable names are unique and clearly describe what information
values the variables hold. **2 =** A minority of variable names are
unique and clearly describe what information values the variables
hold. **1 =** None of the variable names clearly describe what
information values the variables hold.�h]�(h7)��}�(h�**4 =**�h]�h�4 =�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�` All variable names are unique and clearly describe what
information values the variables hold. �����}�(h�` All variable names are unique and clearly describe what
information values the variables hold. �hj�  ubh7)��}�(h�**3 =**�h]�h�3 =�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�l The majority of
variable names are unique and clearly describe what information
values the variables hold. �����}�(h�l The majority of
variable names are unique and clearly describe what information
values the variables hold. �hj�  ubh7)��}�(h�**2 =**�h]�h�2 =�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�j A minority of variable names are
unique and clearly describe what information values the variables
hold. �����}�(h�j A minority of variable names are
unique and clearly describe what information values the variables
hold. �hj�  ubh7)��}�(h�**1 =**�h]�h�1 =�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�X None of the variable names clearly describe what
information values the variables hold.�����}�(h�X None of the variable names clearly describe what
information values the variables hold.�hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hjp  hhhh*hNubeh}�(h]��variable-names�ah!]�h#]��variable names�ah%]�h']�uh)h	hj�  hhhh*hK�ubh
)��}�(hhh]�(h)��}�(h�Mathematical operations�h]�h�Mathematical operations�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(hXb  **4 =** Uses a mathematical operation on at least two variables in a
way that is integral to the program. **3 =** Uses a mathematical
operation on at least one variable in a way that is integral to the
program. **2 =** Uses a mathematical operation incorrectly or not in
a way that is integral to the program. **1 =** No mathematical
operations are used.�h]�(h7)��}�(h�**4 =**�h]�h�4 =�����}�(hhhj   ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�c Uses a mathematical operation on at least two variables in a
way that is integral to the program. �����}�(h�c Uses a mathematical operation on at least two variables in a
way that is integral to the program. �hj�  ubh7)��}�(h�**3 =**�h]�h�3 =�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�b Uses a mathematical
operation on at least one variable in a way that is integral to the
program. �����}�(h�b Uses a mathematical
operation on at least one variable in a way that is integral to the
program. �hj�  ubh7)��}�(h�**2 =**�h]�h�2 =�����}�(hhhj&  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�\ Uses a mathematical operation incorrectly or not in
a way that is integral to the program. �����}�(h�\ Uses a mathematical operation incorrectly or not in
a way that is integral to the program. �hj�  ubh7)��}�(h�**1 =**�h]�h�1 =�����}�(hhhj9  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj�  ubh�% No mathematical
operations are used.�����}�(h�% No mathematical
operations are used.�hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj�  hhhh*hNubeh}�(h]��mathematical-operations�ah!]�h#]��mathematical operations�ah%]�h']�uh)h	hj�  hhhh*hK�ubh
)��}�(hhh]�(h)��}�(h�micro:bit program�h]�h�micro:bit program�����}�(hje  hjc  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj`  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(hX�  **4 =** micro:bit program: ``*`` Uses variables in a way that is
integral to the program ``*`` Uses mathematical operations to add,
subtract, multiply, and/or divide variables ``*`` Compiles and runs
as intended ``*`` Meaningful comments in code **3 =** micro:bit
program lacks 1 of the required elements. **2 =** micro:bit program
lacks 2 of the required elements. **1 =** micro:bit program lacks 3
or more of the required elements.�h]�(h7)��}�(h�**4 =**�h]�h�4 =�����}�(hhhjx  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hjt  ubh� micro:bit program: �����}�(h� micro:bit program: �hjt  ubh �literal���)��}�(h�``*``�h]�h�*�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjt  ubh�9 Uses variables in a way that is
integral to the program �����}�(h�9 Uses variables in a way that is
integral to the program �hjt  ubj�  )��}�(h�``*``�h]�h�*�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjt  ubh�R Uses mathematical operations to add,
subtract, multiply, and/or divide variables �����}�(h�R Uses mathematical operations to add,
subtract, multiply, and/or divide variables �hjt  ubj�  )��}�(h�``*``�h]�h�*�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjt  ubh� Compiles and runs
as intended �����}�(h� Compiles and runs
as intended �hjt  ubj�  )��}�(h�``*``�h]�h�*�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjt  ubh� Meaningful comments in code �����}�(h� Meaningful comments in code �hjt  ubh7)��}�(h�**3 =**�h]�h�3 =�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hjt  ubh�5 micro:bit
program lacks 1 of the required elements. �����}�(h�5 micro:bit
program lacks 1 of the required elements. �hjt  ubh7)��}�(h�**2 =**�h]�h�2 =�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hjt  ubh�5 micro:bit program
lacks 2 of the required elements. �����}�(h�5 micro:bit program
lacks 2 of the required elements. �hjt  ubh7)��}�(h�**1 =**�h]�h�1 =�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hjt  ubh�< micro:bit program lacks 3
or more of the required elements.�����}�(h�< micro:bit program lacks 3
or more of the required elements.�hjt  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjq  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj`  hhhh*hNubeh}�(h]��micro-bit-program�ah!]�h#]��micro:bit program�ah%]�h']�uh)h	hj�  hhhh*hK�ubh
)��}�(hhh]�(h)��}�(h�Collaboration reflection�h]�h�Collaboration reflection�����}�(hj+  hj)  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj&  hhhh*hK�ubj  )��}�(hhh]�h,)��}�(h��**4 =** Reflection piece addresses all prompts. **3 =** Reflection
piece lacks 1 of the required elements. **2 =** Reflection piece
lacks 2 of the required elements. **1 =** Reflection piece lacks 3 of
the required elements.�h]�(h7)��}�(h�**4 =**�h]�h�4 =�����}�(hhhj>  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj:  ubh�) Reflection piece addresses all prompts. �����}�(h�) Reflection piece addresses all prompts. �hj:  ubh7)��}�(h�**3 =**�h]�h�3 =�����}�(hhhjQ  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj:  ubh�4 Reflection
piece lacks 1 of the required elements. �����}�(h�4 Reflection
piece lacks 1 of the required elements. �hj:  ubh7)��}�(h�**2 =**�h]�h�2 =�����}�(hhhjd  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj:  ubh�4 Reflection piece
lacks 2 of the required elements. �����}�(h�4 Reflection piece
lacks 2 of the required elements. �hj:  ubh7)��}�(h�**1 =**�h]�h�1 =�����}�(hhhjw  ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hj:  ubh�3 Reflection piece lacks 3 of
the required elements.�����}�(h�3 Reflection piece lacks 3 of
the required elements.�hj:  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj7  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj&  hhhh*hNubeh}�(h]��collaboration-reflection�ah!]�h#]��collaboration reflection�ah%]�h']�uh)h	hj�  hhhh*hK�ubeh}�(h]��
assessment�ah!]�h#]��
assessment�ah%]�h']�uh)h	hhhhhh*hK�ubeh}�(h]��project-everything-counts�ah!]�h#]��project: everything counts�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�micro:bit top and spin counter�h �substitution_definition���)��}�(h�b.. |micro:bit top and spin counter| image:: /static/courses/csintro/variables/microbit-spinner.png�h]�h�)��}�(hj7  h]�h}�(h]�h!]�h#]�h%]�h']��alt�j@  �uri��6/static/courses/csintro/variables/microbit-spinner.png�uh)h�hj  hh*hK�ubah}�(h]�h!]�h#]�j@  ah%]�h']�uh)j	  hh*hK�hj&  hhub�Duct tape wallet�j
  )��}�(h�T.. |Duct tape wallet| image:: /static/courses/csintro/variables/duct-tape-wallet.jpg�h]�h�)��}�(hjU  h]�h}�(h]�h!]�h#]�h%]�h']��alt�j^  �uri��6/static/courses/csintro/variables/duct-tape-wallet.jpg�uh)h�hj"  hh*hK�ubah}�(h]�h!]�h#]�j^  ah%]�h']�uh)j	  hh*hK�hj&  hhub�Baseball pitch counter�j
  )��}�(h�Z.. |Baseball pitch counter| image:: /static/courses/csintro/variables/baseball-counter.jpg�h]�h�)��}�(hjs  h]�h}�(h]�h!]�h#]�h%]�h']��alt�j|  �uri��6/static/courses/csintro/variables/baseball-counter.jpg�uh)h�hj9  hh*hK�ubah}�(h]�h!]�h#]�j|  ah%]�h']�uh)j	  hh*hK�hj&  hhubu�substitution_names�}�(�micro:bit top and spin counter�j  �duct tape wallet�j!  �baseball pitch counter�j8  u�refnames�}��refids�}��nameids�}�(j�  j�  h�h�j�  j�  j&  j#  jj  jg  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  jm  jj  j�  j�  j]  jZ  j#  j   j�  j�  u�	nametypes�}�(j�  Nh�Nj�  Nj&  Njj  Nj�  Nj�  Nj�  Nj�  Nj�  Nj�  Njm  Nj�  Nj]  Nj#  Nj�  Nuh}�(j�  hh�hdj�  h�j#  h�jg  j)  j�  jm  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  jj  j�  j�  jp  jZ  j�  j   j`  j�  j&  h�h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�(h,)��}�(h�:Cannot analyze code. No Pygments lexer found for "blocks".�h]�h�>Cannot analyze code. No Pygments lexer found for “blocks”.�����}�(hhhjw  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hjt  ubh �literal_block���)��}�(h��.. code:: blocks

   basic.forever(() => {
       basic.showNumber(input.acceleration(Dimension.X))
       basic.showLeds(`
           . . . . .
           . . . . .
           . . . . .
           . . . . .
           . . . . .
       `)
   })
�h]�h��.. code:: blocks

   basic.forever(() => {
       basic.showNumber(input.acceleration(Dimension.X))
       basic.showLeds(`
           . . . . .
           . . . . .
           . . . . .
           . . . . .
           . . . . .
       `)
   })
�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)j�  hjt  hh*ubeh}�(h]�h!]�h#]�h%]�h']��level�K�type��WARNING��line�Ko�source�h*uh)jr  hj�  hhhh*hK{uba�transform_messages�]��transformer�N�
decoration�Nhhub.