����      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�!Unplugged: Binary vending machine�h]�h �Text����!Unplugged: Binary vending machine�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�wC:\Users\nelve\Documents\bitbucket\microbit-introduction-to-block-based-programming\source\csintro\binary\unplugged.rst�hKubh �	paragraph���)��}�(hX�  In this activity, students will explore the concept of binary numbers by
experimenting with a very odd vending machine that only accepts Base-2
coins and doesn’t give change! In the process, students will become
familiar with an alternate numbering system, in this case binary
(Base-2). Students will learn how binary relates to decimal, and will be
able to convert between the two systems.�h]�hX�  In this activity, students will explore the concept of binary numbers by
experimenting with a very odd vending machine that only accepts Base-2
coins and doesn’t give change! In the process, students will become
familiar with an alternate numbering system, in this case binary
(Base-2). Students will learn how binary relates to decimal, and will be
able to convert between the two systems.�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh
)��}�(hhh]�(h)��}�(h�	Materials�h]�h�	Materials�����}�(hh@hh>hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh;hhhh*hKubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�Paper�h]�h,)��}�(hhUh]�h�Paper�����}�(hhUhhWubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhSubah}�(h]�h!]�h#]�h%]�h']�uh)hQhhNhhhh*hNubhR)��}�(h�Pencil�h]�h,)��}�(hhlh]�h�Pencil�����}�(hhlhhnubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhjubah}�(h]�h!]�h#]�h%]�h']�uh)hQhhNhhhh*hNubhR)��}�(h�bSet of ‘coins’ - these could be checkers/chess pieces, cardboard
rounds, or even post-it notes�h]�h,)��}�(h�bSet of ‘coins’ - these could be checkers/chess pieces, cardboard
rounds, or even post-it notes�h]�h�bSet of ‘coins’ - these could be checkers/chess pieces, cardboard
rounds, or even post-it notes�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhhNhhhh*hNubhR)��}�(h�"Vending machine visual (optional)
�h]�h,)��}�(h�!Vending machine visual (optional)�h]�h�!Vending machine visual (optional)�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhhNhhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)hLhh*hKhh;hhubeh}�(h]��	materials�ah!]�h#]��	materials�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Pre-activity preparation�h]�h�Pre-activity preparation�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hKubh,)��}�(hXI  Gather or create small counters or ‘coins’ in the following
denominations: 1, 2, 4, 8, 16, 32 Plastic white poker chips work well as
coins. You can write the denominations onto one side of the coins with a
whiteboard marker. You can also use small index cards or paper squares.
Make sure to leave one side of each coin blank.�h]�hXI  Gather or create small counters or ‘coins’ in the following
denominations: 1, 2, 4, 8, 16, 32 Plastic white poker chips work well as
coins. You can write the denominations onto one side of the coins with a
whiteboard marker. You can also use small index cards or paper squares.
Make sure to leave one side of each coin blank.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�hhubh,)��}�(h��Amount: One set of coins (with one coin of each of the first four
denominations in it) for each student or each pair of students.�h]�h��Amount: One set of coins (with one coin of each of the first four
denominations in it) for each student or each pair of students.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�hhubh,)��}�(h�-Hold onto the 16 and 32 unit coins for later.�h]�h�-Hold onto the 16 and 32 unit coins for later.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK hh�hhubh
)��}�(hhh]�(h)��}�(h�~hint�h]�h�~hint�����}�(hj  hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hK#ubh,)��}�(h�**Tip**�h]�h �strong���)��}�(hj  h]�h�Tip�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK%hh�hhubh,)��}�(hXh  If you have time, create on a poster board, on the whiteboard, or on
paper as a handout, a big rectangle representing a vending machine. Draw
in different items for purchase that would appeal to the students. Have
the different items priced differently from 1 unit to 15 units. This is
particularly good to have for younger and more visually oriented
students.�h]�hXh  If you have time, create on a poster board, on the whiteboard, or on
paper as a handout, a big rectangle representing a vending machine. Draw
in different items for purchase that would appeal to the students. Have
the different items priced differently from 1 unit to 15 units. This is
particularly good to have for younger and more visually oriented
students.�����}�(hj(  hj&  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK'hh�hhubh,)��}�(h�PYou can also just make a very simple vending machine diagram like the
one below:�h]�h�PYou can also just make a very simple vending machine diagram like the
one below:�����}�(hj6  hj4  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK.hh�hhubh,)��}�(h�|Vend-o-matic diagram| ### ~�h]�(h �image���)��}�(h�5image:: /static/courses/csintro/binary/vendomatic.png�h]�h}�(h]�h!]�h#]�h%]�h']��alt��Vend-o-matic diagram��uri��,static/courses/csintro/binary/vendomatic.png��
candidates�}��*�jU  suh)jF  hh*hK�hjB  hhubh� ### ~�����}�(h� ### ~�hjB  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK1hh�hhubeh}�(h]��hint�ah!]�h#]��~hint�ah%]�h']�uh)h	hh�hhhh*hK#ubeh}�(h]��pre-activity-preparation�ah!]�h#]��pre-activity preparation�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Introduction�h]�h�Introduction�����}�(hjy  hjw  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjt  hhhh*hK4ubh,)��}�(hX0  Ask the students the following questions to spark discussion: \* Have
any of them bought anything in the last 24 hours? *Usually they have
bought a snack or perhaps lunch.* \* Did any of them use cash? \* What
bills or coins did they use? \* What are the core denominations of money
in the United States?�h]�(h�wAsk the students the following questions to spark discussion: * Have
any of them bought anything in the last 24 hours? �����}�(h�xAsk the students the following questions to spark discussion: \* Have
any of them bought anything in the last 24 hours? �hj�  hhhNhNubh �emphasis���)��}�(h�4*Usually they have
bought a snack or perhaps lunch.*�h]�h�2Usually they have
bought a snack or perhaps lunch.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh�� * Did any of them use cash? * What
bills or coins did they use? * What are the core denominations of money
in the United States?�����}�(h�� \* Did any of them use cash? \* What
bills or coins did they use? \* What are the core denominations of money
in the United States?�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK6hjt  hhubh,)��}�(h�kLead the students to realize that our core monetary denominations, like
our number system are based on ten.�h]�h�kLead the students to realize that our core monetary denominations, like
our number system are based on ten.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK<hjt  hhubhM)��}�(hhh]�(hR)��}�(h�1 penny�h]�h,)��}�(hj�  h]�h�1 penny�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK?hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h�1 dime = 10 pennies�h]�h,)��}�(hj�  h]�h�1 dime = 10 pennies�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK@hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h�-1 one dollar bill = 10 dimes (or 100 pennies)�h]�h,)��}�(hj�  h]�h�-1 one dollar bill = 10 dimes (or 100 pennies)�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKAhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h�'1 ten dollar bill = 10 one dollar bills�h]�h,)��}�(hj  h]�h�'1 ten dollar bill = 10 one dollar bills�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKBhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h�,1 hundred dollar bill = 10 ten dollar bills
�h]�h,)��}�(h�+1 hundred dollar bill = 10 ten dollar bills�h]�h�+1 hundred dollar bill = 10 ten dollar bills�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKChj  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hK?hjt  hhubh,)��}�(h��Our money system is based on our number system, the decimal system. The
*deci-* prefix means ‘one tenth’. Each place value in the decimal is one
tenth of the place value to its left.�h]�(h�HOur money system is based on our number system, the decimal system. The
�����}�(h�HOur money system is based on our number system, the decimal system. The
�hj4  hhhNhNubj�  )��}�(h�*deci-*�h]�h�deci-�����}�(hhhj=  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj4  ubh�k prefix means ‘one tenth’. Each place value in the decimal is one
tenth of the place value to its left.�����}�(h�k prefix means ‘one tenth’. Each place value in the decimal is one
tenth of the place value to its left.�hj4  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKEhjt  hhubh,)��}�(hXr  **Example:** The amount eleven is written in decimal notation as 11.
There is a numeral one in the ‘tens place’ and a numeral one in the
‘ones place’. The leftmost numeral one in the ‘tens place’ represents
one ten. The next numeral one represents an amount that is one tenth the
amount of the place value to its left; in this case, one tenth of ten,
or one.�h]�(j  )��}�(h�**Example:**�h]�h�Example:�����}�(hhhjZ  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hjV  ubhXf   The amount eleven is written in decimal notation as 11.
There is a numeral one in the ‘tens place’ and a numeral one in the
‘ones place’. The leftmost numeral one in the ‘tens place’ represents
one ten. The next numeral one represents an amount that is one tenth the
amount of the place value to its left; in this case, one tenth of ten,
or one.�����}�(hXf   The amount eleven is written in decimal notation as 11.
There is a numeral one in the ‘tens place’ and a numeral one in the
‘ones place’. The leftmost numeral one in the ‘tens place’ represents
one ten. The next numeral one represents an amount that is one tenth the
amount of the place value to its left; in this case, one tenth of ten,
or one.�hjV  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKIhjt  hhubh,)��}�(h�iBut what is it like to use a different monetary system? A monetary
system that has a base other than ten?�h]�h�iBut what is it like to use a different monetary system? A monetary
system that has a base other than ten?�����}�(hju  hjs  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKPhjt  hhubeh}�(h]��introduction�ah!]�h#]��introduction�ah%]�h']�uh)h	hhhhhh*hK4ubh
)��}�(hhh]�(h)��}�(h�Process�h]�h�Process�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKTubhM)��}�(hhh]�(hR)��}�(h�PGive a set of the coins you prepared earlier to each student or pair
of students�h]�h,)��}�(h�PGive a set of the coins you prepared earlier to each student or pair
of students�h]�h�PGive a set of the coins you prepared earlier to each student or pair
of students�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKVhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h�;Remember to hold onto the 16-unit and 32-unit coins for now�h]�h,)��}�(hj�  h]�h�;Remember to hold onto the 16-unit and 32-unit coins for now�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKXhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h� Present the following scenario:
�h]�h,)��}�(h�Present the following scenario:�h]�h�Present the following scenario:�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKYhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hKVhj�  hhubh �comment���)��}�(hhh]�h}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)j�  hj�  hhhh*hK[ubh �block_quote���)��}�(hhh]�hM)��}�(hhh]�(hR)��}�(h�9There is a vending machine that sells items of all prices�h]�h,)��}�(hj  h]�h�9There is a vending machine that sells items of all prices�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK]hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  ubhR)��}�(h�'However, the machine cannot give change�h]�h,)��}�(hj  h]�h�'However, the machine cannot give change�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK^hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  ubhR)��}�(h�7Therefore, you must pay for everything in exact amounts�h]�h,)��}�(hj/  h]�h�7Therefore, you must pay for everything in exact amounts�����}�(hj/  hj1  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK_hj-  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  ubhR)��}�(h�'You have one of each coin: 1, 2, 4, 8.
�h]�h,)��}�(h�&You have one of each coin: 1, 2, 4, 8.�h]�h�&You have one of each coin: 1, 2, 4, 8.�����}�(hjJ  hjH  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK`hjD  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  ubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hK]hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  hhhNhNubh,)��}�(h�**Questions**�h]�j  )��}�(hjj  h]�h�	Questions�����}�(hhhjl  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hjh  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKbhj�  hhubhM)��}�(hhh]�(hR)��}�(h�CWhat is the price of the least expensive item you can buy? (1 unit)�h]�h,)��}�(hj�  h]�h�CWhat is the price of the least expensive item you can buy? (1 unit)�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKdhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj  hhhh*hNubhR)��}�(h�DWhat is the price of the most expensive item you can buy? (15 units)�h]�h,)��}�(hj�  h]�h�DWhat is the price of the most expensive item you can buy? (15 units)�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKehj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj  hhhh*hNubhR)��}�(h�=What else can you buy? What coin(s) would you use to do this?�h]�h,)��}�(hj�  h]�h�=What else can you buy? What coin(s) would you use to do this?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKfhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj  hhhh*hNubhR)��}�(h�VWhat is the price of something you cannot buy, because you don’t have
exact change?
�h]�h,)��}�(h�UWhat is the price of something you cannot buy, because you don’t have
exact change?�h]�h�UWhat is the price of something you cannot buy, because you don’t have
exact change?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKghj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hKdhj�  hhubh,)��}�(hX.  Here is where students will start to figure out the different combined
sums of different coins. You can also prompt them by saying, for
example, “It’s impossible to buy something that costs 11 units, isn’t
it?” Someone will immediately point out that you CAN buy an 11-unit item
with 8 + 2 + 1.�h]�hX.  Here is where students will start to figure out the different combined
sums of different coins. You can also prompt them by saying, for
example, “It’s impossible to buy something that costs 11 units, isn’t
it?” Someone will immediately point out that you CAN buy an 11-unit item
with 8 + 2 + 1.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKjhj�  hhubh,)��}�(hX  You can now have the students write down how they could pay, what
coin(s) could they use to purchase each of the items priced 1 unit
through 15 units with the coins they have OR have a whole class
discussion with you keeping track of their methods of payment on the
whiteboard.�h]�hX  You can now have the students write down how they could pay, what
coin(s) could they use to purchase each of the items priced 1 unit
through 15 units with the coins they have OR have a whole class
discussion with you keeping track of their methods of payment on the
whiteboard.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKphj�  hhubh,)��}�(h��There will soon be a general agreement among the students that: \* You
can make every amount between 1 unit and 15 units with the 4 coins in
their set \* There is only one way to make each of those amounts.�h]�h��There will soon be a general agreement among the students that: * You
can make every amount between 1 unit and 15 units with the 4 coins in
their set * There is only one way to make each of those amounts.�����}�(h��There will soon be a general agreement among the students that: \* You
can make every amount between 1 unit and 15 units with the 4 coins in
their set \* There is only one way to make each of those amounts.�hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKvhj�  hhubh,)��}�(h�`Have students line up the coins in their set from greatest to least
denomination, left to right.�h]�h�`Have students line up the coins in their set from greatest to least
denomination, left to right.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKzhj�  hhubh,)��}�(h�**Questions**�h]�j  )��}�(hj   h]�h�	Questions�����}�(hhhj"  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK}hj�  hhubhM)��}�(hhh]�(hR)��}�(h��What do you notice about the denominations as they increase from
right to left? *Each amount is double (or times 2 or twice) the
denomination before it (to its right).*�h]�h,)��}�(h��What do you notice about the denominations as they increase from
right to left? *Each amount is double (or times 2 or twice) the
denomination before it (to its right).*�h]�(h�PWhat do you notice about the denominations as they increase from
right to left? �����}�(h�PWhat do you notice about the denominations as they increase from
right to left? �hj<  ubj�  )��}�(h�X*Each amount is double (or times 2 or twice) the
denomination before it (to its right).*�h]�h�VEach amount is double (or times 2 or twice) the
denomination before it (to its right).�����}�(hhhjE  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj<  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhj8  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj5  hhhh*hNubhR)��}�(h��If we added one more coin to your set of coins that is greater than
the 8 unit coin, what is the next logical coin denomination? 16. Why?
*Because 16 is ‘2 times’ greater than 8.*
�h]�h,)��}�(h��If we added one more coin to your set of coins that is greater than
the 8 unit coin, what is the next logical coin denomination? 16. Why?
*Because 16 is ‘2 times’ greater than 8.*�h]�(h��If we added one more coin to your set of coins that is greater than
the 8 unit coin, what is the next logical coin denomination? 16. Why?
�����}�(h��If we added one more coin to your set of coins that is greater than
the 8 unit coin, what is the next logical coin denomination? 16. Why?
�hjc  ubj�  )��}�(h�-*Because 16 is ‘2 times’ greater than 8.*�h]�h�+Because 16 is ‘2 times’ greater than 8.�����}�(hhhjl  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjc  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj_  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj5  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hKhj�  hhubh,)��}�(h�DHand out the 16 unit coins, one to each student or pair of students.�h]�h�DHand out the 16 unit coins, one to each student or pair of students.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h�**Questions**�h]�j  )��}�(hj�  h]�h�	Questions�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubhM)��}�(hhh]�(hR)��}�(h�=What is the new maximum price you could pay for an item? *31*�h]�h,)��}�(hj�  h]�(h�9What is the new maximum price you could pay for an item? �����}�(h�9What is the new maximum price you could pay for an item? �hj�  ubj�  )��}�(h�*31*�h]�h�31�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h�jWhat combinations of coins can you use to pay for an item priced from
16 units to this new maximum price?
�h]�h,)��}�(h�iWhat combinations of coins can you use to pay for an item priced from
16 units to this new maximum price?�h]�h�iWhat combinations of coins can you use to pay for an item priced from
16 units to this new maximum price?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hK�hj�  hhubh,)��}�(hX1  Once again, you can now have the students write down how they could pay,
what coin(s) could they use to purchase each of the items priced 16
units through the new maximum price with the coins they have, OR have a
whole class discussion with you keeping track of their methods of
payment on the whiteboard.�h]�hX1  Once again, you can now have the students write down how they could pay,
what coin(s) could they use to purchase each of the items priced 16
units through the new maximum price with the coins they have, OR have a
whole class discussion with you keeping track of their methods of
payment on the whiteboard.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h��Again, there will soon be a general agreement among the students that:
\* You can make every amount between 16 units and the new maximum with
the 5 coins now in their set. \* There is only one way to make each of
those amounts.�h]�h��Again, there will soon be a general agreement among the students that:
* You can make every amount between 16 units and the new maximum with
the 5 coins now in their set. * There is only one way to make each of
those amounts.�����}�(h��Again, there will soon be a general agreement among the students that:
\* You can make every amount between 16 units and the new maximum with
the 5 coins now in their set. \* There is only one way to make each of
those amounts.�hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h�**Questions**�h]�j  )��}�(hj  h]�h�	Questions�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubhM)��}�(hhh]�hR)��}�(h��If we added one more coin to your set of coins that is greater than
the 16 unit coin, what is the next logical coin denomination? 32.
Why? *Because 32 is ‘2 times’ greater than 16.*
�h]�h,)��}�(h��If we added one more coin to your set of coins that is greater than
the 16 unit coin, what is the next logical coin denomination? 32.
Why? *Because 32 is ‘2 times’ greater than 16.*�h]�(h��If we added one more coin to your set of coins that is greater than
the 16 unit coin, what is the next logical coin denomination? 32.
Why? �����}�(h��If we added one more coin to your set of coins that is greater than
the 16 unit coin, what is the next logical coin denomination? 32.
Why? �hj3  ubj�  )��}�(h�.*Because 32 is ‘2 times’ greater than 16.*�h]�h�,Because 32 is ‘2 times’ greater than 16.�����}�(hhhj<  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj3  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj/  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj,  hhhh*hNubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hK�hj�  hhubh,)��}�(h�DHand out the 32 unit coins, one to each student or pair of students.�h]�h�DHand out the 32 unit coins, one to each student or pair of students.�����}�(hj^  hj\  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h�**Questions**�h]�j  )��}�(hjl  h]�h�	Questions�����}�(hhhjn  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hjj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubhM)��}�(hhh]�(hR)��}�(h�=What is the new maximum price you could pay for an item? *63*�h]�h,)��}�(hj�  h]�(h�9What is the new maximum price you could pay for an item? �����}�(h�9What is the new maximum price you could pay for an item? �hj�  ubj�  )��}�(h�*63*�h]�h�63�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubhR)��}�(h�jWhat combinations of coins can you use to pay for an item priced from
32 units to this new maximum price?
�h]�h,)��}�(h�iWhat combinations of coins can you use to pay for an item priced from
32 units to this new maximum price?�h]�h�iWhat combinations of coins can you use to pay for an item priced from
32 units to this new maximum price?�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hQhj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']�h�h�uh)hLhh*hK�hj�  hhubh,)��}�(h�P|Coins representing binary digits| From coins to binary notation - the
number 45�h]�(jG  )��}�(h�>image:: /static/courses/csintro/binary/binary-place-values.png�h]�h}�(h]�h!]�h#]�h%]�h']��alt�� Coins representing binary digits��uri��5static/courses/csintro/binary/binary-place-values.png�jV  }�jX  j�  suh)jF  hh*hK�hj�  hhubh�. From coins to binary notation - the
number 45�����}�(h�. From coins to binary notation - the
number 45�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(hX�  Once students are comfortable making combinations of numbers, encourage
them to use ones and zeroes to represent the numbers instead. This
number system uses the number 2 as its base (each place is two times the
one before it.) It is called the Base-2 system, or binary system. The
number system we are normally familiar with is the Base-10 system, or
decimal system (each place is ten times the one before it.)�h]�hX�  Once students are comfortable making combinations of numbers, encourage
them to use ones and zeroes to represent the numbers instead. This
number system uses the number 2 as its base (each place is two times the
one before it.) It is called the Base-2 system, or binary system. The
number system we are normally familiar with is the Base-10 system, or
decimal system (each place is ten times the one before it.)�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h��With their coins in a line in descending order from right to left on a
piece of paper, ask students to represent a given number by keeping face
up the coins they would use to make this amount and flipping over or
putting face down the coins not used.�h]�h��With their coins in a line in descending order from right to left on a
piece of paper, ask students to represent a given number by keeping face
up the coins they would use to make this amount and flipping over or
putting face down the coins not used.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h�D**Example:** Ask them to represent the number 45. *See image above.*�h]�(j  )��}�(h�**Example:**�h]�h�Example:�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  ubh�& Ask them to represent the number 45. �����}�(h�& Ask them to represent the number 45. �hj  hhhNhNubj�  )��}�(h�*See image above.*�h]�h�See image above.�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h��They should have the 32, 8, 4, and 1 coins face up and the 16 and 2
coins face down. Ask the students to place a numeral 1 above the coins
that are face up and a numeral zero over the coins that are face down.�h]�h��They should have the 32, 8, 4, and 1 coins face up and the 16 and 2
coins face down. Ask the students to place a numeral 1 above the coins
that are face up and a numeral zero over the coins that are face down.�����}�(hj/  hj-  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h��The ones and zeros they just drew are the binary number version of the
amount represented by the flipped-up coins. For the example: 45 in
Base-10 = 101101 in Base-2�h]�h��The ones and zeros they just drew are the binary number version of the
amount represented by the flipped-up coins. For the example: 45 in
Base-10 = 101101 in Base-2�����}�(hj=  hj;  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h��Practice translating numbers from Base-10 to Base-2 The students can now
use this same method to translate other numbers from Base-10 to Base-2.�h]�h��Practice translating numbers from Base-10 to Base-2 The students can now
use this same method to translate other numbers from Base-10 to Base-2.�����}�(hjK  hjI  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h�4**Examples:** >22 (*1 0 1 1 0* ) 37 (*1 0 0 1 0 1* )�h]�(j  )��}�(h�**Examples:**�h]�h�	Examples:�����}�(hhhj[  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hjW  ubh� >22 (�����}�(h� >22 (�hjW  hhhNhNubj�  )��}�(h�*1 0 1 1 0*�h]�h�	1 0 1 1 0�����}�(hhhjn  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjW  ubh� ) 37 (�����}�(h� ) 37 (�hjW  hhhNhNubj�  )��}�(h�*1 0 0 1 0 1*�h]�h�1 0 0 1 0 1�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjW  ubh� )�����}�(h� )�hjW  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(hX�  Practice translating numbers from Base-2 to Base-10 Next, have the
students use the above method in reverse to translate numbers from
Base-2 to Base-10. \* Start with all the coins face up in a line from
greatest to least denomination from left to right. \* Write the ones and
zeros representing the binary number being translated above the coins.
\* Flip to face down any coin with a zero above it. \* Add up the
remaining face up coins.�h]�hX�  Practice translating numbers from Base-2 to Base-10 Next, have the
students use the above method in reverse to translate numbers from
Base-2 to Base-10. * Start with all the coins face up in a line from
greatest to least denomination from left to right. * Write the ones and
zeros representing the binary number being translated above the coins.
* Flip to face down any coin with a zero above it. * Add up the
remaining face up coins.�����}�(hX�  Practice translating numbers from Base-2 to Base-10 Next, have the
students use the above method in reverse to translate numbers from
Base-2 to Base-10. \* Start with all the coins face up in a line from
greatest to least denomination from left to right. \* Write the ones and
zeros representing the binary number being translated above the coins.
\* Flip to face down any coin with a zero above it. \* Add up the
remaining face up coins.�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubh,)��}�(h�4**Examples:** >0 1 0 1 0 (*10* ) 1 1 0 1 1 0 (*54* )�h]�(j  )��}�(h�**Examples:**�h]�h�	Examples:�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj�  ubh� >0 1 0 1 0 (�����}�(h� >0 1 0 1 0 (�hj�  hhhNhNubj�  )��}�(h�*10*�h]�h�10�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh� ) 1 1 0 1 1 0 (�����}�(h� ) 1 1 0 1 1 0 (�hj�  hhhNhNubj�  )��}�(h�*54*�h]�h�54�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh� )�����}�(h� )�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj�  hhubeh}�(h]��process�ah!]�h#]��process�ah%]�h']�uh)h	hhhhhh*hKTubeh}�(h]�� unplugged-binary-vending-machine�ah!]�h#]��!unplugged: binary vending machine�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�Vend-o-matic diagram�h �substitution_definition���)��}�(h�O.. |Vend-o-matic diagram| image:: /static/courses/csintro/binary/vendomatic.png�h]�jG  )��}�(hjJ  h]�h}�(h]�h!]�h#]�h%]�h']��alt�jS  �uri��-/static/courses/csintro/binary/vendomatic.png�uh)jF  hjY  hh*hK�ubah}�(h]�h!]�h#]�jS  ah%]�h']�uh)jW  hh*hK�hj�  hhub� Coins representing binary digits�jX  )��}�(h�d.. |Coins representing binary digits| image:: /static/courses/csintro/binary/binary-place-values.png�h]�jG  )��}�(hj�  h]�h}�(h]�h!]�h#]�h%]�h']��alt�j�  �uri��6/static/courses/csintro/binary/binary-place-values.png�uh)jF  hjp  hh*hK�ubah}�(h]�h!]�h#]�j�  ah%]�h']�uh)jW  hh*hK�hj�  hhubu�substitution_names�}�(�vend-o-matic diagram�jV  � coins representing binary digits�jo  u�refnames�}��refids�}��nameids�}�(j�  j�  h�h�jq  jn  ji  jf  j�  j�  j�  j�  u�	nametypes�}�(j�  Nh�Njq  Nji  Nj�  Nj�  Nuh}�(j�  hh�h;jn  h�jf  h�j�  jt  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.