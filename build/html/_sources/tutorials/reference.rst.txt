************
References
************

Micro:bit MakeCode Documentation -  https://makecode.microbit.org/docs
	Everything about programming via the MakeCode editor can be found here, including lots of projects.

Kitronik University for BBC Micro:bit - https://www.kitronik.co.uk/blog/bbc-microbit-kitronik-university/
	Great site to learn more about the micro:bit, as well as other editors like Edublocks and MicroPython editor for MicroPython programming.

Scratch Wiki - https://en.scratch-wiki.info/wiki/Blocks
	Some references regarding blocks

Micro:bit official website - https://microbit.org/
	Official site for everything micro:bit

Micro:bit MakeCode editor - https://makecode.microbit.org/
	Official editor for block-based programming

Introduction to Computer Science - https://makecode.microbit.org/courses/csintro
	A 14 week Introduction to Computer Science course.  Included in the Micro:bit MakeCode documentation but just want to highlight it here.

Micro:bit of Things - https://sites.google.com/view/microbitofthings/home
	Micro:bit projects with sensors.  

Micro:bit Developer Community - https://tech.microbit.org/
	A community wiki and a technical data sheet for the micro:bit.  A great site if you want technical details about micro:bit.



