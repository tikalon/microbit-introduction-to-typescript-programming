*****************************************
Micro:Coin : Introduction to Blockchain
*****************************************

Have you heard about BitCoin and all those new Crypto currencies? Well micro:bit has micro:coin now!

.. image:: pictures/microcoin_logo.png
   :scale: 50 %
   :align: center

How Does a Micro:bit Make Coins?
==================================

Each micro:bit contains a block chain, a sequence of blocks, that is public and can’t be modified. Each block represents a coin. To mine new coins, the user shakes the micro:bit and, if they are in luck, their coin added to the chain as a new block! Once the block is added, it is broadcasted to the other micro:bit (the block chain is public and can’t be modified so it’s ok to share it). Other micro:bits receive the block, validate the transaction and update their block chain as needed.

Pressing A shows the number of block you added to the chain, that’s your score. Pressing B shows you the length of the chain.

Happy mining!

Coins, Blocks, Chains
======================

A block chain is a list of blocks that record transactions of a crypto-currency like BitCoin. A block might contain information like the time it was created (mined) and who mined it. The most important part of the block is it’s hash. This is a special number made from the information in the last block of the block list combined with the hash number of previous block in the list. The new block contains information for the current transaction and this new hash number. The new block is added to the list of previous blocks. This list is then transmitted to the crypto currency network. It’s really hard (like impossible) to tamper or forge a hash which allows the block chain to be transmitted publically.


Code
=====

The code uses blocks from ``radio-blockchain package``.

Click on Advanced, then Add Package

.. image:: pictures/microcoin_add_package.JPG
   :scale: 100 %

search for blockchain and add ``radio-blockchain``

.. image:: pictures/microcoin_add_blockchain.JPG
   :scale: 60 %

Here is the code for the micro:coin:

.. image:: pictures/microcoin.JPG
   :scale: 100 %

Try it in the simulator then try it on the micro:bit itself.