.. Introduction to Block Programming using BBC micro:bit documentation master file, created by
   sphinx-quickstart on Sun Sep 23 17:46:40 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction to Block-Based Programming using BBC micro:bit
=============================================================

The tutorial sheets are designed to give students an introduction to the features of the
micro:bit.  Short practical examples are provided and students are invited to design solutions to problems
using the fundamental building blocks presented.

.. toctree::
   :maxdepth:1
   :caption: Tutorials

   tutorials/getting_started
   tutorials/hello-world
   tutorials/images
   tutorials/buttons
   tutorials/accelerometer
   tutorials/compass
   tutorials/thermometer
   tutorials/music
   tutorials/radio
   tutorials/data_types
   tutorials/control
   tutorials/game
   tutorials/data-analysis
   tutorials/app
   tutorials/blockchain
   tutorials/projects
   tutorials/reference





.. Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
