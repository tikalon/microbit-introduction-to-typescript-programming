References
==========

We have included some additional reference books and materials if you
are interested in learning more about the maker mindset.

Maker Education, Physical Computing or Design Thinking in the Classroom
-----------------------------------------------------------------------

`Invent To Learn <http://inventtolearn.com/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **Sylvia Libow Martinez and Gary Stager**

Making, Tinkering, and Engineering in the Classroom

|Invent to Learn Book Cover|

`Launch <http://thelaunchcycle.com/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **John Spencer and AJ Juliani**

Using Design Thinking to Boost Creativity and Bring Out the Maker in
Every Student

|Launch Book Cover|

`The Innovator’s Mindset <http://georgecouros.ca/blog/archives/5715>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **George Couros**

Empower Learning, Unleash Talent, and Lead a Culture of Creativity

|Innovator’s Mindset Book Cover|

`The Big Book of Makerspace Projects <https://colleengraves.org/bigmakerbook/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **Colleen Graves**

Inspiring Makers to Experiment, Create, and Learn

|Makerspace Projects Book Cover|

Code and Computational Thinking
-------------------------------

`Code <http://www.charlespetzold.com/code/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **Charles Petzold**

The Hidden Language of Computer Hardware and Software

|Code Book Cover|

`Girl Code <https://www.girlcodethebook.com/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **Andy Gonzales and Sophie Houser**

Gaming, Going Viral, and Getting it Done

|Girl Code Book Cover|

`Secret Coders <http://www.secret-coders.com/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **Gene Luen Yang and Mike Holmes**

Get with the PROGRAM!

|Secret Coders Book Cover|

`How to Count <https://stevenf.com/books/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

by **Steven Frank**

Programming for Mere Mortals, Vol. 1

|How to Count Book Cover|

Feedback
--------

If you have feedback for the Microsoft MakeCode team, you can fill out
their survey form here: https://aka.ms/microbitfeedback

The support site for the micro:bit is located here:
`https://support.microbit.org <https://support.microbit.org/>`__

.. |Invent to Learn Book Cover| image:: /static/courses/csintro/references/invent-to-learn.jpg
   :target: http://inventtolearn.com/
.. |Launch Book Cover| image:: /static/courses/csintro/references/launch.jpg
   :target: http://thelaunchcycle.com/
.. |Innovator’s Mindset Book Cover| image:: /static/courses/csintro/references/innovators-mindset.jpg
   :target: http://georgecouros.ca/blog/archives/5715
.. |Makerspace Projects Book Cover| image:: /static/courses/csintro/references/makerspace-projects.jpg
   :target: https://colleengraves.org/bigmakerbook/
.. |Code Book Cover| image:: /static/courses/csintro/references/code.jpg
   :target: http://www.charlespetzold.com/code/
.. |Girl Code Book Cover| image:: /static/courses/csintro/references/girl-code.jpg
   :target: https://www.girlcodethebook.com/
.. |Secret Coders Book Cover| image:: /static/courses/csintro/references/secret-coders.jpg
   :target: http://www.secret-coders.com/
.. |How to Count Book Cover| image:: /static/courses/csintro/references/how-to-count.png
   :target: https://stevenf.com/books/
