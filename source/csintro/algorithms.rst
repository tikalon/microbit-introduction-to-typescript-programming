Algorithms
==========

This lesson introduces a conceptual framework for thinking of a
computing device as something that uses code to process one or more
inputs and send them to an output(s).

Lesson objectives
-----------------

Students will…

-  Understand the four components that make up a computer and their
   functions.
-  Understand that the micro:bit takes input, and after processing the
   input, produces output.
-  Learn the variety of different types of information the micro:bit
   takes in as input.
-  Apply this knowledge by creating a micro:bit program that takes input
   and produces an output.

Lesson plan
-----------

1. `Overview: What is a computer and micro:bit
   hardware </courses/csintro/algorithms/overview>`__
2. `Unplugged: What’s your
   function? </courses/csintro/algorithms/unplugged>`__
3. `Activity: Happy face, sad
   face </courses/csintro/algorithms/activity>`__
4. `Project: Fidget cube </courses/csintro/algorithms/project>`__

Related standards
-----------------

`Targeted CSTA standards <algorithms/standards>`__
