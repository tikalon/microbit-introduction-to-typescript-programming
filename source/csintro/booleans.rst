Booleans
========

.. figure:: /static/courses/csintro/booleans/cover.jpeg
   :alt: micro:bit Combo Box

   micro:bit Combo Box

This lesson introduces the use of the boolean data type to control the
flow of a program, keep track of state, and to include or exclude
certain conditions.

Lesson objectives
-----------------

Students will… \* Understand what booleans and boolean operators are,
and why and when to use them in a program. \* Learn how to create a
boolean, set the boolean to an initial value, and change the value of
the boolean within a micro:bit program. \* Learn how to use the random
true or false block. \* Apply the above knowledge and skills to create a
unique program that uses booleans and boolean operators as an integral
part of the program.

Lesson structure
----------------

-  Introduction: Booleans in daily life
-  Unplugged Activity: Two Heads are Better Than One
-  micro:bit Activity: Double Coin Flipper
-  Project: Boolean
-  Assessment: Rubric
-  Standards: Listed

Lesson plan
-----------

1. `Overview: Booleans </courses/csintro/booleans/overview>`__
2. `Unplugged: Two heads are better than
   one </courses/csintro/booleans/unplugged>`__
3. `Activity: Double coin
   flipper </courses/csintro/booleans/activity>`__
4. `Project: Boolean </courses/csintro/booleans/project>`__

Related standards
-----------------

`Targeted CSTA standards </courses/csintro/booleans/standards>`__
