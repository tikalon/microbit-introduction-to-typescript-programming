.. Introduction to Block Programming using BBC micro:bit documentation master file, created by
   sphinx-quickstart on Sun Sep 23 17:46:40 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction to Computer Science using BBC micro:bit 
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   tutorials/getting_started

   SUMMARY
   


.. Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
