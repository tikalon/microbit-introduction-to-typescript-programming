*****************
Accelerometer
*****************

There is an accelerometer on your micro:bit which detects changes in the micro:bit’s speed. It converts analogue information into digital form that can be used in micro:bit programs. Output is in milli-g. The device will also detect a small number of standard actions e.g. shake, tilt and free-fall.

.. image:: pictures/accelerometer.jpg
   :scale: 120 %

The micro:bit measures movement along
three axes:

* X - tilting from left to right.
* Y - tilting forwards and backwards.
* Z - moving up and down.

.. image:: pictures/microbitAxes.jpg

Acceleration
==============
The measurement for each axis is a positive or negative number indicating a value in milli-g's, which is a thousand of a g. A g is as much acceleration as you get from Earth’s gravity.  When the reading is 0 you are "level" along that particular axis.  1024 milli-g is the acceleration due to gravity.

.. image:: pictures/acceleration_block.JPG
   :scale: 100 %

The ``acceleration (mg)`` block below measures the acceleration on a particular axis. ``x`` is the acceleration in the left or right direction, ``y`` is the acceleration in the forward or backward direction and ``z`` is the acceleration in the upward and downward direction.  There is one other parameter, ``strength``, which is the resulting strength of acceleration from all 3 direction.  Mathematically, it is equivalent to this:

.. math::

   strength = \sqrt{x^2 + y^2 + z^2}


Exaample 1 - Bar Chart
-----------------------

This example shows the acceleration of the micro:bit with a bar graph.

.. image:: pictures/acceleration_example1.JPG
   :scale: 100 %

The ``plot bar graph of`` block, which is from the LED section, is a block that plots a bar graph on your LED screen.  It takes in a number, in this case the value from the ``acceleration (mg)`` block and sets a limit for the highest value, in this case 1023.  On your simulator, try to hover your mouse around the virtual micro:bit to see acceleration being plotted on the LED screen.  Try it also on the micro:bit itself by downloading your program and copy it to the micro:bit.


Example 2 - Quake Meter
------------------------

Every 5 seconds, with the micro:bit facing upward on a flat surface, show how much the earth is shaking (if at all).

.. image:: pictures/acceleration_example2.JPG
   :scale: 100 %


Gestures
=========

The really interesting side-effect of having an accelerometer is gesture detection. If you move your BBC micro:bit in a certain way (as a gesture) then it is able to detect it.

The micro:bit is able to recognise the following gestures: ``shake``, ``logo up``, ``logo down``, ``screen up``, ``screen down``, ``tilt left``, ``tilt right``, ``free fall``, ``3g``, or ``6g``. While most of the names should be obvious, the ``3g``, ``6g`` and ``8g`` gestures apply when the device encounters these levels of g-force (like when an astronaut is launched into space).

.. image:: pictures/acceleration_shake.JPG
   :scale: 110 %

The ``on <gesture>`` block will be triggered and executed once a particular gesture, in this case ``shake``, is detected.  


Example 3 - Random Number
--------------------------

This program shows a number from 0 to 9 when you shake the micro:bit.

.. image:: pictures/acceleration_example3.JPG
   :scale: 100 %

On the simulator you can simulate a shake gesture by tapping the small circle besides the SHAKE text.  This will only appear when ``on <gesture>`` block is in the workspace. Then try it also on your micro:bit.

.. image:: pictures/acceleration_simulator.JPG
   :scale: 60 %


Challenges
===========

1. Display the characters 'L' or 'R' depending on whether the BBC micro:bit is tilted to the left or the right.

2. Make the LEDs light up when the strength of the acceleration is greater than 2000 milli-gs.

3. Shake the micro:bit to make the LEDs light up.
