******
Radio
******
Your micro:bit has a radio chip that can be used to transmit and receive messages.

.. image:: pictures/radio.jpg
   :scale: 120 %



Set Radio Group
================

Make a program have the group ID you tell it for sending and receiving with radio. A group is like a cable channel (a micro:bit can only send or receive in one group at a time). A group ID is like the cable channel number.

If you do not tell your program which group ID to use with this function, it will figure out its own group ID by itself. If you load the very same program onto two different micro:bits, they will be able to talk to each other because they will have the same group ID.  

You can set the ID number from 0 to 255.

.. image:: pictures/radio_set_group.JPG
   :scale: 100 %

Example 1 - Set Radio Group
-----------------------------

This program makes the group ID equal 128.

.. image:: pictures/radio_example1.JPG
   :scale: 100 %


Send Number
============

Broadcast a number to other micro:bits connected via radio.

.. image:: pictures/radio_send_number.JPG
   :scale: 100 %


Example 2 - Broadcast Acceleration 
-----------------------------------

This example broadcasts the value of your micro:bit’s acceleration in the x direction (left and right) to other micro:bits. This kind of program might be useful in a model car or model rocket.

.. image:: pictures/radio_example2.JPG
   :scale: 100 %

Example 3 - Light Level Sender
-------------------------------

This example broadcasts the level of the light around it. You can do some interesting things with it if you use it along with the on data packet received example on the next topic.

.. image:: pictures/radio_example3.JPG
   :scale: 100 %


On Radio Received (Number)
===========================

Run part of a program when the micro:bit receives a number over radio.

.. image:: pictures/radio_receive_number.JPG
   :scale: 100 %

Example 4 - Broadcast and Receive Acceleration
------------------------------------------------

This program keeps sending numbers that says how fast the micro:bit is slowing down or speeding up. It also receives numbers for the same thing from nearby micro:bits. It shows these numbers as a bar graph.

.. image:: pictures/radio_example4.JPG
   :scale: 100 %

You can rename the variable ``receivedNumber`` to any name by clicking it.  Try it with 2 micro:bits and see if you can send and receive the acceleration and plot it.


Send String
============

Sends a string to other micro:bits in the area connected by radio. The maximum string length is 19 characters.

.. image:: pictures/radio_send_string.JPG
   :scale: 100 %

Example 5 - Broadcast Codeword
-------------------------------

This program will broadcast the codeword to nearby micro:bits.

.. image:: pictures/radio_example5.JPG
   :scale: 100 %

On Radio Receive (String)
==========================

Run part of a program when the micro:bit receives a string over radio.

.. image:: pictures/radio_receive_string.JPG
   :scale: 100 %

Example6 - Two Way Radio
-------------------------

If you load this program onto two or more micro:bits, you can send a code word from one of them to the others by pressing button A. The other micro:bits will receive the code word and then show it.

.. image:: pictures/radio_example6.JPG
   :scale: 100 %

.. Send Value
	===========

	Send a string and number together by radio to other micro:bits. The maximum string length is 12 characters.

	.. image:: pictures/radio_send_value.JPG
	   :scale: 100 %

	On Radio Receive (String and Number)
	=====================================

	You can simultaneously receive both string and number if the other microbit used the ``radio send value`` block.  

	.. image:: pictures/radio_receive_string_number.JPG
	   :scale: 100 %




Exercises
==========
1. Create a simple text messenger.  Send a message every time button ``A`` is pressed.  Send another message when ``B`` is pressed.

