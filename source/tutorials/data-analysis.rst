**************
Data Analysis
**************

.. image:: pictures/data-analysis.jpg
   :scale: 100 %
   :align: center

We will use values of different types in our micro:bit programs, for example: we could capture acceleration values from the accelerometer. Alternatively, we might want to count the number of button presses the user has made or to show a message to the user telling them the temperature of the room. In order to do these things we need to be able to describe the data we want to use. 

* Numbers - these are integer numbers that you can do operations like addition.
* Strings - these can contain a combination of any characters that we want to treat as text such as letters, numbers and symbols.
* Boolean - used for True and False values.

Plotting data with LEDs
========================

To quickly see and record your data values, use the ``plot bar graph`` block. It will plot a value on the LED screen and write a number to console at the same time.

.. image:: pictures/data_analysis_led_plotting.JPG
   :scale: 100 %


Viewing your Data
==================

When your code is writing data, and the editor is recording it, the Show data button is displayed in the simulator under the board and simulation controls.  You could view your data in a graph or save the data in a csv format. 

.. image:: pictures/data_analysis_data_simulator.JPG
   :scale: 100 %

Writing Data
=============

While you’re using the JavaScript Block Editor, all data written by the serial functions is recorded by the JavaScript Block editor. This happens when you try your code in the simulator and also when the micro:bit is connected to a computer running the MakeCode app with USB.

Data formats
The Data Viewer recognizes the format of your output and decides how to display it. If your data is a stream of values it will plot them in the chart window and show them as text in the console. If your data is just text information, it appears only in the console window.

You can write data in these format types::

   Text (string): "Hello there!"
   Numbers: "354"
   Number arrays: "11,3,45,76"
   Name value pairs: "accelY:956"

Text Output
=============

Text is formed by simply using string data. The text data is not recorded in the console until a complete “line” of text is written. A line is just a string with some special characters (ones you don’t actually see printed) at the end. You can write several strings, say:

.. image:: pictures/data_analysis_write_string.JPG
   :scale: 100 %

This text, though, won’t appear in the editor console until it becomes a complete line. If you follow the string writes with a line in your code, then the line will show up:

.. image:: pictures/data_analysis_write_line.JPG
   :scale: 100 %

Text output is used mostly for messages since number formats have meaning when used for data analysis.

The blank ``serial write line`` adds the special line ending characters and turns the previous strings into a complete line of text. The whole line could simply be written out with just one function:

.. image:: pictures/data_analysis_write_line2.JPG
   :scale: 100 %

When you’re writing only text, it appears only in the console view and not in the chart window. This example writes four messages of text:

.. image:: pictures/data_analysis_example1.JPG
   :scale: 100 %

The four messages appear in the console window:

.. image:: pictures/data_analysis_example1_output.JPG
   :scale: 100 %

Writing Numbers
================

In order for the Data Viewer to recognize a number as value and show in the chart, it has to be in a line by itself. So, numbers written individually won’t be charted:

.. image:: pictures/data_analysis_write_number.JPG
   :scale: 100 %

The numbers don’t show up as single values because they appear in the output as a string: "123". Also, the string doesn’t form a complete line so it doesn’t show up in the console window either. You could add a blank line to the numbers alreay written. If you did this, you would have just one value charted which is 123:

.. image:: pictures/data_analysis_write_number2.JPG
   :scale: 100 %

Here’s a way to chart the numbers individually:

.. image:: pictures/data_analysis_write_number3.JPG
   :scale: 100 %

Number Arrays
==============

Numbers in arrays are displayed on separate data lines on the same chart. You can use serial write numbers to write several values at once. The numbers are written to the output as comma separated values (CSV). The array of numbers:

.. image:: pictures/data_analysis_number_arrays.JPG
   :scale: 100 %

is written to the output in the form of a CSV string as: "0,1,2,3,4".

The Data Viewer recognizes this as an array of numbers and charts them on separate data lines:

.. image:: pictures/data_analysis_number_arrays2.JPG
   :scale: 100 %

Data lines are shown for each value in the array:

.. image:: pictures/data_analysis_number_arrays3.JPG
   :scale: 100 %

Give this example a try and watch the chart make a diamond pattern from two triangle waves:

.. image:: pictures/data_analysis_number_arrays4.JPG
   :scale: 100 %

It will look like this:

.. image:: pictures/data_analysis_number_arrays5.JPG
   :scale: 100 %

Name Value Pairs
=================

A very common way to report and record data is to use a name value pair (NVP). A value is given a name so you know what it’s for or where it came from. The name value pair is written with the format of name:value. There is a name, a colon character, and the value all together on one line. The line is written to the output and the Data Viewer recognizes it as a value and charts it as part of a value stream. The value stream is based on the name so any new values received with the same name are displayed on the chart that’s showing values for that name. If more than one type of name value pair is found, the Data Viewer makes another value stream and shows those values on a different chart.

If you want to report the values for both temperature and light, you can make separate name value pairs for them. The name value pairs are written using serial write value. This function writes the values to the output as lines in a format like this:

.. image:: pictures/data_analysis_value_pairs.JPG
   :scale: 100 %

Two charts display each value stream:

.. image:: pictures/data_analysis_value_pairs2.JPG
   :scale: 100 %

The console output shows the different name value pairs too:

.. image:: pictures/data_analysis_value_pairs3.JPG
   :scale: 100 %

Writing Subvalues
==================

Similar to number arrays, different name value pairs are shown on one chart by using subvalues. You make a subvalue by combining a first-level name and a second-level name with a '.': ``"firstLevel.secondLevel"``

The first-level name is value name for the chart. The second-level name is the name for the actual value displayed.

If you want to show all three values of acceleration on a single chart, then use each axis as a second-level (subvalue) name, ``"acceleration.x"``.

.. image:: pictures/data_analysis_subvalue.JPG
   :scale: 100 %

To show the values for each axis together:

.. image:: pictures/data_analysis_subvalue2.JPG
   :scale: 100 %

Each subvalue 'x', 'y', and 'z' is displayed on the chart named "acceleration" in the Data Viewer.

.. image:: pictures/data_analysis_subvalue3.JPG
   :scale: 100 %

Generating data
================

Sensor Values
---------------

Most of the data you want to record probably comes from values measured by the sensors. The sensor values are read using ``Input`` blocks. They return to your program the current value measured by the sensor.

.. image:: pictures/data_analysis_sensors.JPG
   :scale: 100 %

Pin Data
---------

External sensors and devices connected to the board are read using the Pins blocks. Here are some examples of reading the pins and reporting measurements from devices:

.. image:: pictures/data_analysis_pin_data.JPG
   :scale: 100 %

Human Events
-------------

Sometimes we want to track human interactions with a device. These events might be button presses, gestures, or pin touches.

Example 2 - Button Tracker
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Run this example in the editor and switch to the Data Viewer. Watch it plot your button presses like pulse events in the chart.

.. image:: pictures/data_analysis_example2.JPG
   :scale: 100 %

Writing an additional value creates another stream that will appear in a separate chart. Adding and event for another button press, we can plot pulses on a second chart.

.. image:: pictures/data_analysis_example2a.JPG
   :scale: 100 %

Time and Timestamps
---------------------

A timestamp marks when you read a value or detect that an event happens. These are commonly written along with other data values as an additional data item. The data viewer will create timestamps for values it sees when you write them. If you are downloading and saving data from the Data Viewer, it creates the timestamps for you and you don’t need to add them.

If you need your own time values, you can make them from the running time of the board.

.. image:: pictures/data_analysis_timestamps.JPG
   :scale: 100 %

Also, you can write your own custom CSV with your own timestamp:

.. image:: pictures/data_analysis_timestamps2.JPG
   :scale: 100 %

Remote Data Collection
=======================

If you have more than one micro:bit you can setup one of them to receive data sent by radio from other micro:bits. Remote micro:bits can take measurements and send them to a board that’s connected by USB to a computer. The micro:bit connected to the computer is the data recorder and writes the recieved data to the serial port.

.. image:: pictures/data_analysis_remote_data.JPG
   :scale: 100 %

Receiver micro:bit
-------------------

Connect the micro:bit to a computer with a USB cable. The data received over radio is sent to the computer with this connection using writes to the serial port. If you have the Windows 10 MakeCode app, you can view the received data with the Data Viewer in the editor. Otherwise, you need a serial terminal app or some other program that can read from the computer’s serial port.

The receiving micro:bit sets a radio group number on which to listen for incoming messages.

.. image:: pictures/data_analysis_remote_data1.JPG
   :scale: 100 %

The receiver then waits to receive a packet (radio message) from the sender which contains the data to record. This happens inside an ``on radio received`` block. If the sending micro:bit is measuring temperature at a remote location (somewhere else in the room maybe), the receiver will write the value received as a temperature measurement to the serial port.

.. image:: pictures/data_analysis_remote_data2.JPG
   :scale: 100 %

A remote micro:bit reads its measurement values and sends them to the same radio group as the receiver.

.. image:: pictures/data_analysis_remote_data1.JPG
   :scale: 100 %

A typical measurment progam might read a sensor value continously. Depending on how much the values change, the meaurement program could contain the read operation in a loop with a delay interval. In the example here, the delay is one minute between each read of a temperature value. The value is sent on the current radio group with ``radio send number``.

.. image:: pictures/data_analysis_remote_data3.JPG
   :scale: 100 %

Remote Sender Micro:bit
-------------------------

A remote micro:bit reads its measurement values and sends them to the same radio group as the receiver.

.. image:: pictures/data_analysis_remote_data1.JPG
   :scale: 100 %

A typical measurment progam might read a sensor value continously. Depending on how much the values change, the meaurement program could contain the read operation in a loop with a delay interval. In the example here, the delay is one minute between each read of a temperature value. The value is sent on the current radio group with ``radio send number``.

.. image:: pictures/data_analysis_remote_data4.JPG
   :scale: 100 %


Challenges
==========

1.  Create an seismograph (earthquake detector) that will plot the movement of the micro:bit in all direction and plot it on the editor using the data simulator.  Also record the timestamp.