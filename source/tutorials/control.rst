******************
Control Structures
******************

Sometimes you need to control what happens in your program, do you need to repeat something over and over again? Loops will help you there. Perhaps you only want something to happen if a particular event has occurred? Take a look at conditions to help you there. Of course, in any program you will likely use all of these to make your micro:bit do what you want and that's where things get interesting.

.. image:: pictures/control_structures_icons.jpg
   :align: left

You have already seen how to program sequentially and generated input and output. In the remainder of this chapter we will look at conditionals and loops.

If Statement
==============

Conditionally run code depending on whether a Boolean condition is true or false.

.. image:: pictures/controls_if.JPG
   :scale: 100 %

Click on the dark blue gear icon (see above) to add an else or else if to the current block.

.. image:: pictures/controls_ifelse.JPG
   :scale: 100 %

As discussed on the earlier chapter, the blocks on the ``then`` section will only be executed if the if statement is true.  It will go to each of the ``else if`` section of the block if the condtion above is false.  The ``else`` section is the last section of the block, when all the other conditions above are false.  You can add several ``else if`` sections but you can only add one ``else`` and only at the end of the block.  

Example 1 - Comparisons of Numbers
-----------------------------------

When you compare two Numbers, you get a Boolean value, such as the comparison x < 5 in the code below:

.. image:: pictures/controls_example1.JPG
   :scale: 100 %

In this example, if the variable ``x`` is not greater than 5, it will execute the blocks on the ``then`` section, if it is false, it will execute the blocks on the ``else`` section.

Example 2 -  Music in the Dark
-----------------------------

If the ``light level`` is < (lesser than) 100, this code will play a dadadum tune when button A is pressed.

.. image:: pictures/controls_example2.JPG
   :scale: 100 %

Here in this example you can use the LED screen as a light detector by using the ``light level`` block.  Its value ranges from 0 (dark) to 255 (bright).

Loops
=====
There are three types of loop blocks: ``repeat`` loop block is a simple loop that let you perform an action for a number of times, ``for`` loop block let you count the number of times you do something and ``while`` loopblock  which let you perform an action until a condition you've specified is no longer happening. 

Repeat
-------
Run part of the program the number of times you say.

.. image:: pictures/controls_repeat.JPG
   :scale: 100 %

On the previous chapter, we encounter this type of loop block.

.. image:: pictures/music_example4.JPG
   :scale: 100 %

While
-------

Repeat code while a Boolean condition is true.

.. image:: pictures/controls_while.JPG
   :scale: 100 %

The while loop has a condition that evaluates to a Boolean value.

The condition is tested before any code runs. Which means that if the condition is false, the code inside the loop doesn’t execute.


Example 3 - Diagonal Line
^^^^^^^^^^^^^^^^^^^^^^^^^^

The following example uses a while loop to make a diagonal line on the LED screen (points [0, 0], [1, 1], [2, 2], [3, 3], [4, 4]).

.. image:: pictures/controls_example3.JPG
   :scale: 100 %

For Statement
--------------

Run part of the program the number of times you say.

.. image:: pictures/controls_for.JPG
   :scale: 100 %

Example 4 - Count to 5
^^^^^^^^^^^^^^^^^^^^^^^

This program will show the numbers 0, 1, 2, 3, 4 and 5 one after another on the LED screen.

.. image:: pictures/controls_example4.JPG
   :scale: 100 %


Challenges
============

1.  Display the Fibonacci series up to the 10th number when you press the button B. Fibonacci series is defined as a series of numbers in which each number ( Fibonacci number ) is the sum of the two preceding numbers. The simplest is the series 0, 1, 1, 2, 3, 5, 8, etc.  

2.  Make a coin flipping program that will display either heads or tails.  Use random blocks (under Math section) to determine the outcome of the coin flip (50% chance of heads or tails). 

