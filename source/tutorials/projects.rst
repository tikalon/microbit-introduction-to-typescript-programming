*****************************
Projects
*****************************

In this chapter, you will apply what you learn on the previous chapters by doing some porjects.  Good Luck!


Project 1: Soil Moisture
============================

.. image:: pictures/soil-moisture.jpg
   :scale: 60%
   :align: center

Track the soil moisture of your plants.


Materials
-----------

- 1 micro:bit with battery pack and batteries
- 2 long nails or soil moisture module
- 2 crocodile clips


Make
------

Here’s what you need to do to make your soil moisture sensor:

- Connect a nail to the 3V pin with a croc clip and insert it into the soil.
- Connect the other nail to the P0 pin with a croc clip and insert it into the soil.

.. image:: pictures/soil-moisture_setup1.jpg
   :scale: 80%
   :align: center

That’s it!


Code
-----

You will code your moisture meter using a pot of dry dirt and wet dirt. This is so you can set the micro:bit to know what both dry and wet conditions are.

Step 1: Measuring Moisture
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The soil itself has some electrical resistance which depends on the amount of water and nutrients in it. It acts like a variable resistor in an electronic circuit. The water is not conductive but the nutrient content is. The combination of water and soil nutrients makes the soil have some conductivity. So, the more water there is, combined with the nutrients, the less the soil will have electrical resistance.

To measure this, we read the voltage on pin **P0** using ``analog read pin`` which returns a value between 0 (no current) and 1023 (maximum current). The value is graph on the screen using ``plot bar graph``.

.. image:: pictures/soil-moisture2.jpg
   :scale: 100%
   :align: center


Experiment!
~~~~~~~~~~~~

Insert the nails in the dry dirt and you should see most LEDs turn off.
Insert the nail in the wet dirt and you should see most LEDs turn on.


Step 2: Sensor data values
^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the previous program, we only have a rough idea of what the sensor value is. It’s using just a tiny screen to display it! Let’s add code that displays the current reading when button **A** is pressed.

This code needs to go into the ``forever`` loop. We’ve also added the variable reading to store the reading value.

.. image:: pictures/soil-moisture3.jpg
   :scale: 100%
   :align: center


Experiment!
~~~~~~~~~~~~

Insert the nails in the dry dirt, press A and note the value. You should see a value close to around 250for dry dirt.
Insert the nails in the wet dirt, press A and note the value. You should see a value somewhere near 1000 for wet dirt.


Step 3: Don’t waste energy!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We want our soil probes to work for a long time and to save our battery power, so we need to tweak our code so our moisture sensor doesn’t use too much energy.

- Our circuit connects directly to the **3V** pin so it is always using electricity. Instead, we will connect it to **P1** and turn that pin **high** only while the measurement is taken. This saves electricty and also avoids corrosion of the probes.

.. image:: pictures/soil-moisture_setup2.jpg
   :scale: 80%
   :align: center

- We will also lower the brightness of the screen to lower the energy consumption from the LEDs.
- Soil moisture changes very slowly so we don’t need to measure it all the time!!! Let’s add a **sleep** of 5 seconds in the loop as well.

.. image:: pictures/soil-moisture4.jpg
   :scale: 100%
   :align: center


Experiment!
~~~~~~~~~~~~

Using the dry soil and wet soil pots, test that your circuit still works. Remember you’ll have to wait up to 10 seconds to see a change!

You can access the project online by clicking this `link <https://makecode.microbit.org/projects/soil-moisture/code>`_.


Project 2: Telegraph
=====================

Build a telegraph between two micro:bits to communicate with your friends!

Materials
----------

- 2 micro:bits, a battery holder, and 2 AAA batteries
- 4 crocodile clips


Make
-----

Step 1
^^^^^^^

.. image:: pictures/telegraph1.png
   :scale: 60%
   :align: center

Connect the end of the 1st crocodile clip to the GND pin on the micro:bit.


Step 2
^^^^^^^

.. image:: pictures/telegraph2.png
   :scale: 60%
   :align: center

Connect the end of the 2nd crocodile clip to the 3V pin on the micro:bit.


Step 3
^^^^^^^

.. image:: pictures/telegraph3.png
   :scale: 60%
   :align: center

Connect the end of the 3rd crocodile clip to pin 1 of the micro:bit.


Step 4
^^^^^^^

.. image:: pictures/telegraph4.png
   :scale: 60%
   :align: center

Connect the end of the 4th crocodile clip to pin 2 of the micro:bit.


Step 5
^^^^^^^

.. image:: pictures/telegraph5.png
   :scale: 60%
   :align: center

Connect the other end of the 1st crocodile clip to the GND on the second micro:bit.


Step 6
^^^^^^^

.. image:: pictures/telegraph6.png
   :scale: 60%
   :align: center

Connect the other end of the 2nd crocodile clip to the 3V pin on the second micro:bit.


Step 7
^^^^^^^

.. image:: pictures/telegraph7.png
   :scale: 60%
   :align: center

Connect the other end of the 3rd crocodile clip to pin 2 of the second micro:bit.


Step 8
^^^^^^^

.. image:: pictures/telegraph8.png
   :scale: 60%
   :align: center

Connect the other end of the 4th crocodile clip to pin 1 of the second micro:bit.


Step 9
^^^^^^^

.. image:: pictures/telegraph9.png
   :scale: 60%
   :align: center

Here is the complete setup:

.. image:: pictures/telegraph_setup.jpg
   :scale: 60%
   :align: center


Code
-----

Let’s build the code that, when the user presses the button A on a micro:bit, will send an impulse over a wire to the receiving micro:bit and turn on an LED there.

Make sure that the sending and receiving wires run “symmetrically” between the two boards. That is: pin 1 on one micro:bit is connected to pin 2 on the other, and vice versa. Just like it’s shown in the pictures in the make section. This way we can use the same code on both micro:bits .


Step 1
^^^^^^^

We start with a block that digitally writes high value (a digital 1) to P1 which sends the value to micro:bit’s pin 1. This block is found in Pins drawer of the Advanced section of the Toolbox.

.. image:: pictures/telegraph_code1.JPG
   :scale: 100%
   :align: center


Step 2
^^^^^^^

To show that we are sending the 1, we add a block to turn on an LED in the center of the LED display (2, 2) using ``plot x y``:

.. image:: pictures/telegraph_code2.JPG
   :scale: 100%
   :align: center


Step 3
^^^^^^^

Now that we know how to send the signal, we only want to do it while the button A is pressed. Pick an ``if then else`` block from the **Logic** drawer (you’ll leave the ``else`` part empty for now). Add a check for when button **A** is pressed. Get an ``on button pressed`` from the **Input** drawer and move the blocks from the previous step into ``then`` part of the ``if then else``:

.. image:: pictures/telegraph_code3.JPG
   :scale: 100%
   :align: center


Step 4
^^^^^^

For the ``else`` section (while button **A** is not pressed) we want to do the opposite of what we did in the ``then`` section. Which is, make the value of pin ``P1`` go to **low** (digital 0) and unplot the corresponding LED on the sending micro:bit:

.. image:: pictures/telegraph_code4.JPG
   :scale: 100%
   :align: center


Step 5
^^^^^^^

Let’s wrap it all in a forever loop so this code is running in the background always checking button A and sending the appropriate signal to the receiver. Modify your code to add the blocks below. Download the code onto one of the micro:bits, press and release button A a few times.

.. image:: pictures/telegraph_code5.JPG
   :scale: 100%
   :align: center

The sending part is done, so now we’ll add the receiving part.


Step 6
^^^^^^^

The receiver needs to digitally read from the pin where the other micro:bit sends its value to pin 2 across the wire. Let’s start by going to the Pins drawer, adding a digital read pin and change the pin value to P2.

Now, we want to examine the value read from P2 and check whether it’s high (1) or low (0). Go to the Logic drawer and pick an if then else block, then come back for the comparison operator 0 = 0. Plug in our digital read pin block as one operand and the value 1 as the other.

We’ll turn the LED in the bottom right corner (4, 4) on to show that we received a high value and turn it off in not.

Make sure your code looks like this:

.. image:: pictures/telegraph_code6.JPG
   :scale: 100%
   :align: center

Your telegraph is ready!


Step 7
^^^^^^^

Ok, let’s try it out:

1. Connect the first micro:bit to your computer using your USB cable and download the telegraph code to it.
2. Disconnect the first micro:bit.
3. Connect the second micro:bit to your computer using your USB cable and download the telegraph code to it.
4. Disconnect the second micro:bit.
5. Connect the battery holder to one of the micro:bits.
6. The first person, and then second person, can take turns pressing button A on their own micro:bits to play the telegraph game!



Project 3: Bedside Light
=========================

.. image:: pictures/bedside_light.jpg
   :scale: 80

This is a very simple project to introduce the children to sensing and control which makes use of an LDR as a light sensor and an LED for the bedside light. 
This also introduces the concept of an analogue input. A digital input is either ON or OFF. There are only two possible conditions. An analogue input is one from a range of possible values. In the case of the microbit, from 0 to 
Once these principles are understood, the tasks can be re-imagined to fit hundreds of different scenarios. The bedside light could become a street light, or emergency lighting on the inside of an aircraft etc. etc.. 


Materials
---------

- 1 micro:bit with battery pack and batteries
- 1 Light Dependent Resistor (LDR)
- 1 LED light
- 2 Resistor
- 4 crocodile clips
- 3-4 jumper wires
- materials for the bedside light structure


Make
-----

.. image:: pictures/bedside_light1.jpg
   :scale: 50%
   :align: center

A light dependent resistor (LDR) can be used as the light sensor.  An LDR does not operate like a simple on/off switch. As the light level is reduced, the LDR's resistance increases and the voltage flowing through it  to pin 2 decreases.
The script must read the analogue value between pin 2 and GND.  For reasons that will be explained in the Science section of this website, a fixed resistor is connected between 3 volts and pin 2 on the micro:bit.  The resistance of R2 should be similar to the resistance of the LDR.

But do not worry if you don't understand all of that, just follow the diagram on the right and pictures below.

.. image:: pictures/bedside_light2.jpg
   :scale: 70%
   :align: center

.. image:: pictures/bedside_light3.jpg
   :scale: 70%
   :align: center

.. image:: pictures/bedside_light4.jpg
   :scale: 100%
   :align: center

   By experimentation, it was discovered that an analogue input of  greater than 950 is a good setting to turn the bedside light on.
This experimentation or calibration activity is an excellent way for children to develop an understanding of analogue control systems. The optimum values will vary depending on the resistance of the LDR used and the light level required to switch the LED on and off.

An LED (with a limiting resistor R1) connected between pin 1 and GND is used for the light.

You can constuct the bedside ligjt structure to house your micro:bit, wires and LED and LDR after everything is working.


Code
-----

Here is the MakeCode block code for this project:

.. image:: pictures/bedside_light5.jpg
   :scale: 100%



Project 4: Traffic Light
==========================

.. image:: pictures/traffic-light.jpg
   :scale: 70%

The traffic light project is a bit like "Hello world!" of control technology. It is a great project to get children started. They are all familiar with the traffic light and fully understand what it needs do. 
Half the battle with any project you teach in school is finding a context that is relevant to the children's lives and that they fully understand. Even better if that context serves a useful function or solves a practical problem.

Materials
---------

- 1 micro:bit with battery pack and batteries
- 1 traffic light module with Red, Amber and Green lights
- 4 crocodile clips
- materials to constuct the structure for the traffic light


Make
-----

.. image:: pictures/traffic-light-setup.JPG
   :scale: 60%

All you need for the model is a red, yellow and green LED with about 30 cm of wire attached to each leg of the LED.
To make it easier for the children to correctly connect the LEDs to the pins:
- Use a red wire (on the longer leg of the LED) to indicate positive - this will connect to the pin side of the circuit.
- Use a black wire (on the shorter leg of the LED) to indicate negative - this will connect to the GND side of the circuit.

.. image:: pictures/traffic-light1.jpg
   :scale: 80%


Code
-----

Here is the algorithm::

	green light on (cars may pass)
	pedestrian light at stop (x sign)
	when pedestrian presses button (A)
		wait for 3 seconds
	    green light off, amber light on
	    pause for 4 seconds
	    amber light off, red light on
	    pedestrian sign at go (walking man animation)
	    pause for 10 seconds (to allow pedestrians to cross)
	    red light off, amber light blinking for 2 seconds
	    amber light off, green light on again

Here is the MakeCode program:

.. image:: pictures/traffic-light_code.JPG
   :scale: 100%


Project 5: Burglar Alarm
=========================

.. image:: pictures/burglar-alarm.jpg
   :scale: 100%


Burglar alarms and security systems are well understood by the children who are aware that they serve a valuable function. This makes them a superb context for control projects. 
Working on security systems is very engaging for children, particularly when they get to test them on their friends.
The selection of a sensor is often a starting point. The simplest sensor of all is some form of on/off switch. The switch provides a digital input. Usually on (1 or high) when triggered.  A push to make switch works best.


Materials
---------

- 1 micro:bit with batteries and battery holder
- 1 passive infrared resistor (PIR)
- 1 speaker or buzzer
- 5 crocodile clips


Make
------

Many intruder alarm systems make use of a PIR or passive infra red sensor as a motion detector.  In fact it is an electronic sensor that measures infrared (IR) light radiating from objects in its field of view. PIR sensors are increasingly used to control automated lighting systems to prevent lights being left on. This is another project you may wish to set for your children. To created an automated light system.

.. image:: pictures/burglar-alarm-setup.JPG
   :scale: 70%


The PIR component has three pins for connecting it to the circuit. Two are for power and should be connected negative to GND and positive to 3V. The third is for the signal wire and if using the script below should be connected to pin0.

The speaker or buzzer is activated when there is movement.  The short end of the buzzer is the **GND** and the other one is the signal, which is to be connected to one of the pins of the micro:bit. 

Construct the structure for your traffic light and the pedestrian light.


Code
-----

1.  Read analog signal on pin1.  The higher the reading (max is 1024), the higher the voltage.  You might need to calibrate the value first, so you need to check what will be the value if there is a movement.  Based on my experiment, I got higher than 200 when there is movement.  Different PIR might have different results so make sure to try yours.

2.  Compare measured value with your threshold value based on your calibration.  This will determine if you need to turn off or on the LED.

3.  Send a high signal ``(.write_digital(1))`` to pin0 to create a sound when there is movement, and a low signal if there is no movement.

Here is the code:

.. image:: pictures/burglar-alarm_code.JPG
   :scale: 100%



Project 5: Simple Calculator
==============================

.. image:: pictures/calculator.png
   :scale: 30%

Let's create a simple calculator that adds two numbers.  We will use the buttons to enter the number.


Materials
---------

- 1 microbit with battery pack and batteries


Make
-----

We will just be using the micro:bit, so no other materials.


Code
-----

We will use button **A** to enter the first number.  Pressing the button will increment the number from 0.  
We will use button **B** to enter the second number.  Pressing the button will increment the number from 0.  
We will use the button **A+B** to add the two numbers.

Here is the code:

.. image:: pictures/simple_calculator_code.JPG
   :scale: 100%



Porject 6: Lighthouse
=======================

.. image:: pictures/lighthouse.jpg
   :scale: 100%

The lighthouse is another great project for young children for the same reasons as the traffic light. They are all familiar with a lighthouse and fully under stand what it needs do. 
Half the battle with any project you teach in school is finding a context that is relevant to the children's lives and that they fully understand. Even better if that context serves a useful function.

The task is to create a lighthouse model that can sense light and dark and automatically operate a light to flash on for 1 second and off for three seconds when it is dark.  It must also have a buzzer for a fog horn that will sound for 2 seconds then switch off for 2 seconds when a button is pressed.


Materials
----------

- 1 micro:bit with battery pack and batteries
- 1 light dependent resistor (LDR) for light sensing
- 1 speaker or buzzer for the fog horn
- 1 resistor
- materials for the lighthouse structure


Make
------

.. image:: pictures/lighthouse_setup.jpg
   :scale: 80%

To make this model, all you need is an LED, an LDR, the tube from a kitchen roll (or you can make one from cardboard) and a transparent cup, to make the lighthouse. For the rocks you will need some paper and PVA glue, a  buzzer and a push switch. 
You will need to add about 40 cm of wire to the legs of the LED and the LDR and 20 cm of wire to the buzzer and push switch.
The connector blocks are to make the connection between the lighthouse model and the ``gpiO`` interface.

.. image:: pictures/lighthouse_model.jpg
   :scale: 100%


Code
-------

This is the algorithm for the light house.::

   Repeat forever
        if input is high
             switch the LED on
             pause for 1 second
             switch the LED off
             pause for 3 seconds
        else switch the LED off
        if the push switch is closed
             switch the buzzer on
             pause for 2 seconds
             switch the buzzer off
             pause for 2 seconds

This is the MakeCode block program:

.. image:: pictures/lighthouse_code.jpg
   :scale: 100%



Project 7: Step Counter
========================

Also known as a pedometer, a step counter is a device, usually portable and electronic or electromechanical, that counts each step a person takes by detecting the motion of the person's hands, hips or legs. Because the distance of each person's step varies, an informal calibration, performed by the user, is required if presentation of the distance covered in a unit of length (such as in kilometers or miles) is desired, though there are now pedometers that use electronics and software to automatically determine how a person's step varies.  Used originally by sports and physical fitness enthusiasts, pedometers are now becoming popular as an everyday exercise counter and motivator.

.. image:: pictures/step-counter.jpg
   :scale: 80 %

You will be needing something to hold the micro:bit to your ankle as you will strap this to detect step count.  Display the step count whenever a step is detected.  Use the accelerometer ``shake`` gesture for this. Also use the button **A** to check for the current step count. Use button **B** to reset the micorbit.


Materials
-----------

- 1 micro:bit with battery holder and batteries
- materials for securing micro:bit to your ankles


Make
-----

To secure the micro:bit and its components on the ankle, you will need materials such as tape and cardboard (or any flexible material), or you can attach it to the bottom part of the pants by sewing it.  


Code
-----

Here is the algorithm for the step counter::

   initialize step counter to zero
   show welcome screen on display
   if 'shake' gesture detected
      increment step counter
      display image that step detected
   if button A is pressed
      display number of steps
      pause for 500 ms

Here is the code:

.. image:: pictures/step-counter_code.jpg
   :scale: 100 %



Project 8: Area of the Triangle
================================

In this project you will use your micro:bit to solve the area of the triangle.  

.. image:: pictures/area-of-triangle.png
   :scale: 100 %

Area of triangle is defined as

.. math::

   Area = 0.5 * base * height


Materials
-----------

- 1 micro:bit


Make
-----

We will only use the micro:bit for this project, so no other materials needed.


Code
------

We will take first the value of base by incrementing using the button **A**.  Button **B** will be use to confirm the number.  It will then get the value of height using the same method.  After confirming the value for height, it will display the area of the triangle and repeat again from the start.::

   Welcome message
   initialize value for base and height
   Ask for value of base
   Increment value of base until confirmed by pressing button B
   Store value of base
   Ask for value of height
   Increment value of height until confirmed by pressing button B
   Store value of height
   Get the area of triangle using the formula above.
   Show value for area
   Repeat first step

Here is the block code for this project:

.. image:: pictures/area_traingle1.jpg
   :scale: 100 

.. image:: pictures/area_traingle2.jpg
   :scale: 100 


Project 9: Rock Paper Scissors
===============================

.. image:: pictures/rock-paper-scissors.jpg
   :scale: 100 %

Make the Rock-Paper-Scissors game on your micro:bit and challenge your friends.


Materials
----------

- 1 micro:bit, battery holder and 2 AAA batteries
- materials to strap the micro:bit to your wrist


Make
------

Use materials that will hold the micro:bit and its battery pack to your wrist, such as large tape and velcro.


Code
------

You will use the gesture 'shake' to activate the micro:bit and display either rock, paper or scissors.  You will be using the ``pick random`` block to choose between 0 to 2 and assign to each hand.

Here is the code:

.. image:: pictures/rock-paper-scissors_code.jpg
   :scale: 80 %