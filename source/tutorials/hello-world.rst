************
Hello World!
************

Your First Program
===================
Coding using the micro:bit is composed of these 4 steps. You can expect to go around the loop  quite a few times before you get your code working.

.. image:: pictures/microbit_lifecycle.jpg
   :scale: 60%
   :align: center


Design the Code
----------------

First of all you are going to write a program to display the message “Hello World!” followed by an image on the display of your micro:bit. There's not much planning and design to do here, but just so that you understand what a plan might look like::

    My Plan
    1. Display "Hello World!!"
    2. Display a heart image
    3. Wait for 3 seconds
    4. Go back to Step 1

Write the Code 
---------------
Using the JavaScript Blocks Editor, write your first code by drag and drop blocks and try your program on the simulator, like the one shown below:

.. image:: pictures/hello_world.JPG
   :scale: 100%
   :align: center

Let's go through each block elements:

.. image:: pictures/hello_world_block_forever.JPG
   :scale: 100%
   :align: center

The ``forever`` block will repeat all the blocks inside this forever.  Blocks are executed from top to bottom.  This means if all the blocks inside the forever block were executed, it will repeat from the start.  Note the shape of the block, this type of block is called an **event block**.  In this case, ``forever`` event means that this will be executed always, so the blocks inside it will be executed indefinitely.

.. image:: pictures/hello_world_block_string.JPG
   :scale: 100%
   :align: center

The ``show string`` block will display the words inside the quotation marks, also known as strings, on the LED display.  Note the shape of the block, this block is a **stack block**, they will perform the main command, in this case, to show string.

.. image:: pictures/hello_world_block_icon.JPG
   :scale: 100%
   :align: center

The ``show icon`` block will display a built-in image in the LED display.  You can choose among the different selection by clicking the icon on the editor. 

.. image:: pictures/hello_world_block_pause.JPG
   :scale: 100%
   :align: center

The ``pause (ms)`` block will pause the micro:bit for the 3000 milliseconds, or 3 seconds. 

.. image:: pictures/hello_world_simulator.JPG
   :scale: 100%
   :align: center

Notice on the left side, there is a virtual micro:bit running your code at the same time.  This is the simulator that we discussed on the previous chapter.  It will execute your code on the simulator, just like the real thing.  This is so that you could try your code first before running it on the real thing.  You could interact with the micro:bit simulator like pushing the buttons and shaking the microbit.  There are small buttons below the simulator where you can stop and play the micro:bit simulator, slo-mo the execution, mute an audio if there is an audio playing on your micro:bit (this will be discussed on later chapters), and expand the simulator to fullscreen.


Download the Code
-------------------

Click the Download button in the editor. This will download a 'hex' file, which is a compact format of your program that your micro:bit can read. Once the hex file has downloaded, copy it to your micro:bit just like copying a file to a USB drive. On Windows you can right click and choose "Send To→MICROBIT."

.. image:: pictures/send_to_microbit2.jpg
   :scale: 60%
   :align: center

Or you can drag the hex file to the MICROBIT drive to download the code to your micro:bit.

.. image:: pictures/drag_to_copy.JPG
   :scale: 100%
   :align: center

You have written your first program. Carry on and see what else you can do with the micro:bit.

Challenge
==========

1.  Make a name tag by displaying your name on the LED screen.
