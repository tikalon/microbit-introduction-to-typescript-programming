***********************
Solutions to Exercises
***********************

Micro:bit - Getting Started
============================

1.  A *micro:bit* is a pocket sized computer that you can make all sorts of projects like musical instruments or games.

2.  A *magnetometer* can detect the direction the micro:bit is facing, like a compass.

3.  A micro:bit can be powered on by *2x AAA Batteries* or *through USB*.

4.  What are the 4 steps in coding the micro:bit. *Design*, *Edit/Coding*, *Compile and Copy/Flash to micro:bit* and *Test*.

5.  *Block-Based Programming* is a coding with the programming language where instructions are represented by blocks.

6.  *False*.  To get your code to the micro;bit, you must download it clicking the download button on the *lower left* side of teh editor.

7.  Block-based programming uses blocks, text-based programming uses text.


Hello World!
============

1.  Here is the solution code

.. code-block:: python

	from microbit import *

	while True:
		display.scroll('John')
		sleep(2000)
		display.scroll(15)
		sleep(1000)

2.  Answer: The display on the micro:bit will be updated to 4.

3.  Answer:  You will see the value of 5 printed on the REPL window


LED Display
============

1.  Here is one of the solution

.. code-block:: python

	from microbit import *

	display.show(STICKFIGURE)
	sleep(1000)

2.  Here is the solution

.. code-block:: python

	from microbit import *

	while True:
    	display.show(Image.ALL_ARROWS,delay=200)

To change the speed of animation, change delay to another value

3.  Here is one of the solution

.. code-block:: python

	from microbit import *

	while True:
	    image1 = Image("00000:"
	                  "00000:"
	                  "00000:"
	                  "00000:"
	                  "00000")
	    image2 = Image("50000:"
	                   "55000:"
	                   "50500:"
	                   "50050:"
	                   "55555")
	    image3 = Image("90000:"
	                   "9900:"
	                   "90900:"
	                   "90090:"
	                   "99999")
	    image4 = Image("50000:"
	                   "55000:"
	                   "50500:"
	                   "50050:"
	                   "55555")
	    image5 = Image("00000:"
	                  "00000:"
	                  "00000:"
	                  "00000:"
	                  "00000")
	    all_image = [image1, image2, image3, image4, image5]
	    display.show(all_image,delay=300)

4.  Here is the solution

.. code-block:: python

	from microbit import *

	while True:
	    # display pixel at bottom of screen
	    display.set_pixel(2,4,9)
	    if button_a.is_pressed():
	        # range = 9 is the total number of frames for the jumping animation
	        for i in range(9):
	            # abs operator means absolute value 
	            y = abs(4-i)
	            display.set_pixel(2,y,9)
	            sleep(100)
	            # clears display every frame
	            display.clear()

Buttons
========

1.  Here is the solution

.. code-block:: python

	from microbit import *

	i=0
	j=0
	display.show(Image.PACMAN)
	sleep(100)
	while True:
	    if pin0.is_touched():
	        sum=i+j
	        display.show("=")
	        sleep(200)
	        display.show(sum)
	        sleep(300)
	        i,j=0,0
	    elif button_a.is_pressed():
	        i = i+1
	        display.show(str(i))
	        sleep(500)
	    elif button_b.is_pressed():
	        j=j+1
	        display.show('+' +str(j))
	        sleep(50)

2.  Here is the solution

.. code-block:: python

	from microbit import *

	vote_A = 0
	vote_B = 0
	display.scroll("Vote? A-ham B-eggs")
	while True:
	    if button_a.is_pressed() and button_b.is_pressed():
	        display.scroll("ham= " +str(vote_A) + " eggs= " + str(vote_B))
	    elif button_a.is_pressed():
	        vote_A = vote_A + 1
	        display.show(Image.YES)
	        sleep(100)
	        display.scroll('ham')
	    elif button_b.is_pressed():
	        vote_B = vote_B + 1
	        display.show(Image.YES)
	        sleep(100)
	        display.scroll('eggs')
	    sleep(500)

Accelerometer
=============

1.  Solution

.. code-block:: python

	from microbit import *

	while True:
	    if accelerometer.get_x() < -200:
	        display.show(Image.ARROW_W)
	    elif accelerometer.get_x() > 200:
	        display.show(Image.ARROW_E)
	    else:
	        display.show(Image.SQUARE_SMALL)
	    sleep(300)

2.  Solution

.. code-block:: python

	from microbit import *
	import math

	while True:
	    x = accelerometer.get_x()
	    y = accelerometer.get_y()
	    z = accelerometer.get_z()
	    acceleration = math.sqrt(x**2 + y**2 + z**2)
	    if acceleration > 2000:
	        display.show(Image.SQUARE)
	        sleep(500)
	    else:
	        display.set_pixel(2,2,9)
	    sleep(100)
	    display.clear()

3.  Solution

.. code-block:: python

	from microbit import *

	while True:
	    # checks if there was a 'shake' gesture since the last was_gesture()
	    if accelerometer.was_gesture('shake'):
	       display.show(Image.HEART_SMALL)
	       sleep(1000)
	    else:
	        display.clear()

4.  Solution

.. code-block:: python

	from microbit import *
	import random

	while True:
	    if button_a.was_pressed():
	       display.show(random.randrange(1,7))
	       sleep(500)

Compass
========

1.  Solution

.. code-block:: python

	from microbit import *

	while True:
	    direction = compass.heading()
	    if direction < 45 or direction > 315:
	        display.show(Image.ARROW_N)
	    elif direction < 135 and direction >= 45:
	        display.show(Image.ARROW_W)
	    elif direction < 225 and direction >= 135:
	        display.show(Image.ARROW_S)
	    elif direction <= 315 and direction >= 225:
	        display.show(Image.ARROW_E)

2.  Use the command ``compass.calibrate`` to do this.  Magnetic forces from metals or 
electronic gadgets will affect the calibration.


Thermometer
===========

1.  Temperature reading should change, but it will largely depend on the temperature of the chip in the micro:bit.
Turned on for extended period of time will make the reading become higher than the surroundings.

2.  Solution

.. code-block:: python

	from microbit import *

	while True:
	    temp = temperature()
	    display.show(str(temp) + 'C')
	    sleep(500)

3.  Here is the solution

.. code-block:: python

	from microbit import *

	while True:
	    temp_C = temperature()
	    temp_F = temp_C * 1.8 + 32
	    temp_K = temp_C + 273
	    display.scroll(str(temp_C)+'C')
	    if button_a.was_pressed():
	        display.scroll(str(temp_F)+'F')
	    elif button_b.was_pressed():
	        display.scroll(str(temp_K)+'K')
	    sleep(100)

Music
======

2. Solution

.. code-block:: python

	from microbit import *
	import music

	while True:
	    # checks only the acceleration on x axis
	    acc = accelerometer.get_x()
	    # length of tone, negative number means continuous
	    length = -1
	    music.pitch(abs(acc), length)

Radio
======

1.  Solution

.. code-block:: python

	from microbit import *
	import radio


	radio.on()
	radio.config(channel=19)        # Choose your own channel number
	radio.config(power=7)           # Turn the signal up to full strength

	my_message = "YOLO!"
	another_message = "What's up?"

	# Event loop.
	while True:
	        if button_a.was_pressed():
	            radio.send(my_message)
	            display.show(Image.YES)
	            sleep(500)
	        if button_b.was_pressed():
	            radio.send(another_message)
	            display.show(Image.YES)
	            sleep(500)
	        incoming = radio.receive()
	        if incoming is not None:
	            display.show(incoming)
	            print(incoming)
	        sleep(500)

2.  Solution

.. code-block:: python

	from microbit import *
	import radio


	radio.on()
	radio.config(channel=19)        # Choose your own channel number
	radio.config(power=7)           # Turn the signal up to full strength

	my_message = "YOLO!"
	another_message = "What's up?"

	# Event loop.
	while True:
	        temp = radio.receive()
	        if temp is not None:
	            display.show("Temp=" + str(temp))
	            print("Temp=" + str(temp))
	        sleep(300)

3.  Solution

micro:bit in PC receiving the message

.. code-block:: python

	from microbit import *
	import radio


	radio.on()
	radio.config(channel=19)        # Choose your own channel number
	radio.config(power=7)           # Turn the signal up to full strength

	# Event loop.
	while True:
	        temp = radio.receive()
	        if temp is not None:
	            display.show("Temp=" + str(temp))
	            print("Temp=" + str(temp))
	        sleep(300)

micro:bit sending the temperature

.. code-block:: python

	from microbit import *
	import radio


	radio.on()
	radio.config(channel=19)        # Choose your own channel number
	radio.config(power=7)           # Turn the signal up to full strength

	# Event loop.
	while True:
	        temp = temperature()
	        radio.send(str(temp))
	        display.show(temp)
	        sleep(300)

Control Structures
===================

1. Solution

.. code-block:: python

	from microbit import *
	import math

	def fib(max_num):
	    a,b=0,1
	    for i in range(max_num):
	        display.scroll(a)
	        if b > max_num:
	            return
	        sleep(500)
	        a,b = b,b+a
	fib(200)
	display.show(Image.YES)

2. Solution

.. code-block:: python

	from microbit import *
	import random

	# welcome message
	display.show(Image.HEART)
	sleep(500)

	while True:
	    if button_a.was_pressed():
	        flip = random.randint(0,1)
	        if flip:
	            # display head, represented by a skull
	            display.show(Image.SKULL)
	        else:
	            # display tails, represented by a small square
	            display.show(Image.SQUARE_SMALL)
	        sleep(300)

Data Types
===========

1. Solution

.. code-block:: python

	from microbit import *
	import random

	# welcome message
	display.show(Image.HEART)
	sleep(500)

	number = []
	for i in range(20):
	    number.append(random.randint(2,100))

	for num in number:
	    for i in range(2,num):
	        if num%i == 0:
	            display.scroll(str(num)+" is not prime")
	            sleep(1000)
	            break
	    else:
	        display.scroll(str(num)+" is a prime")
	        sleep(1000)

2. Solution

.. code-block:: python

	from microbit import *

	# welcome message
	display.show(Image.HEART)
	sleep(500)

	count = 0
	message = "Python is the best programming language in the world!"
	vowel = ["a","e","i","o","u","A","I","O","U"]

	# convert message into list, each elemet is a letter
	mess = list(message)

	# iterate on each element on list
	for letter in mess:
	    # iterate on each vowel
	    for v in vowel:
	        if v == letter:
	            count = count+1

	display.scroll("vowels="+str(count))

3.  Solution

.. code-block:: python

	from microbit import *
	import random

	value = [0,]
	while True:
	    value[0] = random.randint(0,1000)
	    tuple1 = tuple(value)
	    print(tuple(tuple1))
	    sleep(300)
	    del tuple1

