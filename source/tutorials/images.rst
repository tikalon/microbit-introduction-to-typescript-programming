***********
LED Display
***********

This is a quick guide to some of the things you can do with the LED display. The idea is that you can use this information to experiment and create something for yourselves. Try stuff out – see what happens and see what you can make.
There are 25 LEDs  set out like the picture below. You can use the LEDs like a very tiny screen to display a single character, a string of characters or a small picture like the smiley face here. Let's find out how to use them.


.. image:: pictures/happy.png
   :scale: 100 %


Example 1 - Yet Another Hello World Program
=============================================

Consider an example below:

.. image:: pictures/image_example.JPG
   :scale: 100 %

This program will show on the micro:bit's LED screen the number '1' first, then a scissor, Hello World! text and a moon.

You will notice on your simulator that it play first the ``show number`` block on the ``on start`` block before executing the ``forever`` block.  The ``on start`` block will be executed only once at start.  

The ``show number`` block will show on the LED screen the number written after the text, in this case it is 1. 

The next one is the ``show icon``.  This will display the chosen icon on the LED screen.  In this example, we chose a scissor icon to be shown.  There are tons of built-in images to choose from.  Here are some of the examples.

.. image:: pictures/images_icons.JPG
   :scale: 80 %

The ``show string`` block will display the "Hello World!" text on the screen.  You might be wondering what is a string.  A string, informally, is a sentence, a bit of text, or a series of characeters, usually written inside a quote.  Both "Hello World" and 'Hello World' are valid strings. 

The ``show leds`` block will display the exact LED will light up as arrange on the matrix.  You can choose which LED on the 5x5 matrix by clicking on the empty cell so that it will turn red.  You can 'draw' anything on the matrix, like the moon image as seen on the example.



Example 2 - Count Up
======================

Consider another example below:

.. image:: pictures/images_countup.JPG
   :scale: 80 %

This program will count from 0 every 1000 millisecond/1 second and display on the LED screen on your micro:bit.

Inside the ``on start`` block there is a new block named ``set``.  This block will set the variable ``item`` to 0. The mathematical equivalent of this is *item = 0*.  This is to initialize a variable to a certain number.  If the variable is not initialize, the editor is smart enough to add a line to initialize the variable to 0.  But do note that the best practice in programming is always initialize a variable to a certain value.

Inside the ``forever`` block, the variable ``item`` was used instead of a number in the ``show number`` block.  The ``show number`` block will accept only numbers such as a literal number like 0, or a variable that contains a number, such as the variable ``item``.  If you use a string, even though it contains a number, such as "1", since it is bounded by quotes, or contains a non-numeric character, it will not be accepted by the editor.  Notice also that the variable ``item`` is also a block, which can be found inside the **Variable** block.  These blocks contains a number, a string, or variable that contains either of the two.

Another block that is new is the ``change`` block.  This block will increment the variable ``item`` by a certain number, in this case, 1.  The mathematical equivalent of this is 

.. math::

  item = item + 1


Example 3 - Flashing Heart
===========================

This time we will make a flashing heart animation.

1.  Place the ``show leds`` block in the on ``start`` block and draw a heart.  Watch the simulator as it executes your program.

.. image:: pictures/flashing_heart_step1.JPG
   :scale: 90 %

2.  Click **Download** to transfer your code into your micro:bit!
3.  Place another show leds block under the heart to make it blink.

.. image:: pictures/flashing_heart_step3.JPG
   :scale: 90 %

4.  Move the blocks inside the forever to repeat the animation.

.. image:: pictures/flashing_heart_step4.JPG
   :scale: 90 %

5.  Click **Download** to transfer your code in your micro:bit and watch the hearts flash!
6.  Place more show leds blocks to create your own animation.

.. image:: pictures/flashing_heart_step6.JPG
   :scale: 90 %

7.  Click **Download** to transfer your code in your micro:bit!


Challenge
==========
1. Create an image on the LEDs and try to animate to see what they look like. 

