************
Game
************

Make games with sprites. Keep score and controls gameplay.

.. image:: pictures/game.jpg
   :scale: 50 %

Create Sprite
==============

Create a new LED sprite pointing to the right.

A sprite is like a little LED creature you can tell what to do. You can tell it to move, turn, and check whether it has bumped into another sprite.

.. image:: pictures/game_create_sprite.JPG
   :scale: 100 %

Parameters
-----------

**x**: The left-to-right place on the LED screen where the sprite will start out.
**y**: The top-to-bottom place on the LED screen where the sprite will start out.
0 and 4 mean the edges of the screen, and 2 means in the middle.

.. note::

   Once the game engine is started, it will render the sprites to the screen and potentially override any kind of animation you are trying to show. Using game pause and game resume to disable and enable the game rendering loop.

Move
======

Move the sprite the number of LEDs you say.  The number is how many LEDs the sprite should move.

.. image:: pictures/game_move.JPG
   :scale: 100 %

Turn
=====

Turn the sprite as much as you say in the direction you say.  You can set to turn left or right, and by how many degrees.

.. image:: pictures/game_turn.JPG
   :scale: 100 %

Example 1
----------

This program starts a sprite in the middle of the screen. Next, the sprite turns toward the lower-right corner. Finally, it moves two LEDs away to the corner.

.. image:: pictures/game_example1.JPG
   :scale: 100 %

Delete
========

Delete a sprite from the game.

.. image:: pictures/game_delete.JPG
   :scale: 100 %

Touching Edge
==============

Find whether the sprite is touching the edge of the LED screen.

Sprites are touching the edge if they overlap with an LED on the edge of the screen.  It will return a Boolean (true or false).

.. image:: pictures/game_touching_edge.JPG
   :scale: 100 %

Set Score
==========

Sets the current score of the game.

.. image:: pictures/game_set_score.JPG
   :scale: 100 %

Add Score
==========

Add more to the current score for the game.

.. image:: pictures/game_change_score.JPG
   :scale: 100 %


Example 2 - A Simple Game
--------------------------

This program is a simple game. Press button A as much as possible to increase the score. Press B to display the score and reset the score.

.. image:: pictures/game_example2.JPG
   :scale: 100 %

Game Over
==========

End the game and show the score.

.. image:: pictures/game_over.JPG
   :scale: 100 %

Example 3 - Pick a Button
--------------------------

This program asks you to pick a button. If you press button A, the program says YOU WIN!. If you press button B, it shows an animation and ends the game.

.. image:: pictures/game_example3.JPG
   :scale: 100 %


Example 4 - Crappy Bird
========================

Let's create a Crappy Bird game! This is based on a popular game on mobile.  Don't let the bird reaches the edge (bottom and top) while it falls downward. Press the A button to move the bird up by 1 LED.

.. image:: pictures/game_example4.JPG
   :scale: 100 %

Advanced
========

There are other functions that were not covered here, you can explore them under the Game section on the block selection of the editor.


Challenges
-----------

1.  Improve the Crappy Bird game by making the bird fall faster as you reach certain score.

