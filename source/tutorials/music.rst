****************
Music
****************
This is a quick guide to some of the things you can do with micro:bit music. The idea is that you can use this information to experiment and create something for yourselves.  You can use the micro:bit to play simple tunes, provided that you connect a headphone or a speaker to your board. 

If you are using a speaker, you can connect your micro:bit using crocodile clips like this: 

.. image:: pictures/buzzer-connect.png
   :scale: 50 %

.. warning:: You cannot control the volume of the sound level from the micro:bit. Please be very careful if you are using headphones. A speaker is a better choice for work with sound.

If you are using headphones you can use crocodile clips to connect your micro:bit to some headphones like this: 

.. image:: pictures/connect_headphones.jpg
   :scale: 70 %


Start a Melody
================

Begin playing a musical melody through pin P0 of the micro:bit.  Let's play some music!

.. image:: pictures/music_start_melody.JPG
   :scale: 100 %

.. note::
	On the simulator, this function only works on the micro:bit and in some browsers.

There are a number of built-in melodies you can choose from:

.. image:: pictures/music_start_melody2.JPG
   :scale: 100 %

Example 1 - Happy Birthday!
-----------------------------

This example plays the Happy Birthday built-in melody.  Try it on the simulator but make sure your computer has a speaker.  Then try it on the micro:bit.

.. image:: pictures/music_example1.JPG
   :scale: 100 %

On the simulator, when you use any of the music blocks, it will show you how to connect your headphones to the micro:bit.  This also means that running the simulator will produce the sound on your computer's speaker.

.. image:: pictures/music_simulator.JPG
   :scale: 100 %


Play Tone
==========

Play a musical tone through pin P0 of the micro:bit for as long as you say.

.. image:: pictures/music_play_tone.JPG
   :scale: 100 %

Clicking the tone (default is Middle C) will pop-up a piano window where you can choose the tone.  Also you can adjust the the beat, or how long the tone will last.

.. image:: pictures/music_play_tone1.JPG
   :scale: 100 %


Instead of selecting the tone, you can also supply the specific frequency (how high or low the tone is) in Hz (Hertz).

.. image:: pictures/music_play_tone_frequency.JPG
   :scale: 100 %


You can also change the pin used to generate music:

.. image:: pictures/music_play_tone_set_pin.JPG
   :scale: 100 %

Example 2 - Play a Tone
------------------------

Play a Middle A tone for half a beat.

.. image:: pictures/music_example2.JPG
   :scale: 100 %

Rest
=====

Rest (play no sound) through pin PO for the amount of time you say.

.. image:: pictures/music_rest_block.JPG
   :scale: 100 %


Set Tempo
==========

Makes the tempo (speed of a piece of music) as fast or slow as you say.

.. image:: pictures/music_set_tempo.JPG
   :scale: 100 %


Example 3 - Play a Tone Again
------------------------------

Play a Middle A tone for half a beat, this time with the tempo set at 160.

.. image:: pictures/music_example3.JPG
   :scale: 100 %

You will notice that the speed of the music increase as you increase the tempo.  Try other values to see the effect on the speed of music.


Example 4 - Star Wars Theme
----------------------------

Play the Star Wars Theme when you press button A.

.. image:: pictures/music_example4.JPG
   :scale: 100 %

You would noticed that we used a ``repeat`` block on this example.  This block will repeat the blocks inside it for a number of times.  We will discuss further these types of block on later chapters.  



Challenges
===========
1. Make up your own tune. 

2. Make a musical instrument. Change the frequency of the sound played based on the readings from the accelerometer.  
