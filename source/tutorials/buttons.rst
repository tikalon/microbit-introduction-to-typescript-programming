***********
Buttons 
***********

The micro:bit has two buttons: labelled ``A`` and ``B``, and 3 large pins at the bottom labeled as pins ``0``, ``1`` and ``2``, which corresponds to P0, P1 and P2 respectively.

.. image:: pictures/microbit_button.jpg
   :scale: 100 %

You can use the buttons to get input from the user.  For the pins, you can get input from user by touching the pins and the GND at the same time. Perhaps you'd like to start or stop your program with a button press or when one of the pins were touched.

Example 1 - Check Which Button was Pressed
===========================================

Consider the following program below:

.. image:: pictures/buttons_example1.JPG
   :scale: 90 %

You will notice a number of **event blocks** in this program, these will be executed depending on the event.  

One of these is the ``on button A pressed`` block, when the physical button A is pressed on the micro:bit all the blocks under it will be executed.  On this example, the ``show string "A"`` and ``pause (ms) 200`` will be executed.

Same thing for ``on button B pressed``, when physical button B is pressed on the micro:bit, the ``show string "B"`` and ``pause (ms) 200`` will be executed.  

Another **event block** you noticed is the ``on pin P0 pressed``, this event will be triggered when you touch and release the pin ``0`` within 1 second while pressing the ``GND`` pin.  

Same thing for the other two event blocks, ``on pin P1 pressed`` and ``on pin P2 pressed``, these events will be triggered when you touch and release pin ``1`` and ``2`` respectively, while holding the ``GND`` pin.

Also when using ``on button A+B pressed``, a new button will appear on the simulator so that you can simulate the buttons A and B pressed together.

.. image:: pictures/buttons_simulator.JPG
   :scale: 60 %

.. note:: 
	
	When doing this on the simulator, instead of pressing both the pins and the GND, just click on the pins.  

Example 2 - Count Button Clicks
================================

This example counts how many times you press the A button. Each time you press the button, the LED screen shows the count variable getting bigger.

.. image:: pictures/buttons_example2.JPG
   :scale: 90 %


Example 3 - Roll Dice
======================

This example shows a number from 1 to 6 when you press the B button.

.. image:: pictures/buttons_example3.JPG
   :scale: 90 %

You will notice a new block colored purple.  These are math blocks and you can find these blocks in Math category.

.. image:: pictures/math_blocks.JPG
   :scale: 90 %

The ``pick random`` block's function is to randomly select a number between 0 and the number you entered, in this case 5. You will also noticed that this block is inside another block that adds the value of the ``pick random`` block and 1. You can change the operator of this block to addition (+), subtraction (-), multiplication (x), division (&#247;) and exponential (**).  This is so that the number that will be stored to the variable ``dice`` will be from 1 to 6.

To change the variable from the default ``item`` to ``dice``, you need to click on ``item`` inside the ``set`` block and select rename variable.  Then a popup window will appear for you to change the variable name to dice.

.. image:: pictures/buttons_example3_rename_variable.JPG
   :scale: 90 %

.. image:: pictures/buttons_example3_rename_variable2.JPG
   :scale: 90 %

Example 4 - Pin Pressed Counter
================================

This program counts how many times you press the P2 pin. Every time you press the pin, the program shows the number of times on the screen.

.. image:: pictures/buttons_example4.JPG
   :scale: 90 %

 
Challenge
==========

1. Create a voting machine where you can select between 2 foods by a press of a button (button a for ham
and button b for eggs, for example).  To display the current tally, press both a and b at the same time.
