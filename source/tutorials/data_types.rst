************
Data Types
************

We will use values of different types in our micro:bit programs, for example: we could capture acceleration values from the accelerometer. Alternatively, we might want to count the number of button presses the user has made or to show a message to the user telling them the temperature of the room. In order to do these things we need to be able to describe the data we want to use. 

* **Numbers** - these are integer numbers that you can do operations like addition.
* **Strings** - these can contain a combination of any characters that we want to treat as text such as letters, numbers and symbols.
* **Boolean** - used for True and False values.

Number
======
An integer number.

A Number is an integer such as 42 or -42. More precisely, a Number is a signed 32-bit integer (an integer value that contains a positive or negative sign as part of it’s information).

Declare a number variable
^^^^^^^^^^^^^^^^^^^^^^^^^^

You can assign a number to a variable:

.. image:: pictures/data_number.JPG
   :scale: 100 %

Arithmetic operators
^^^^^^^^^^^^^^^^^^^^^

The following arithmetic operators work on numbers and return a Number:

.. image:: pictures/data_operators.JPG
   :scale: 100 %

Functions that return a number
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some functions return a number, which you can store in a variable.

For example the following code gets the display brightness (using the brightness function) and stores the value in a variable named brightness:

.. image:: pictures/data_functions.JPG
   :scale: 100 %


String
========

A String is a sequence of characters.  Do note that only digits, letters, punctuation marks and a few symbols are supported in micro:bit.  All other character codes appear as a ? on the LED screen.

Create a string variable
-------------------------

To create a variable that holds a string:

- Click Variables (in the Block drawer).

- Type a name for your new string variable by clicking the down arrow, then click New Variable. Then type the variable name “salutation”

- Drag a string block on the right side of the operator.

- Click "Hello" and then type a string like hello.

Boolean
========

A Boolean has one of two possible values: true or false. Boolean (logical) operators (and, or, not) take Boolean inputs and make another Boolean value. Comparison operators on other types (numbers, strings) create Boolean values.

These blocks represent the true and false Boolean values, which can plug into anyplace a Boolean value is expected:

.. image:: pictures/controls_boolean.JPG
   :scale: 100 %

The next three blocks represent the three Boolean (logic) operators:

.. image:: pictures/controls_operators.JPG
   :scale: 100 %

The next six blocks represent comparison operators that yield a Boolean value. Most comparisons you will do involve numbers:

.. image:: pictures/controls_comparison.JPG
   :scale: 100 %

Boolean values and operators are often used with an ``if`` or ``while`` statement to determine which code will execute next. 


Array
======

An array is a list of items that are numbers, booleans, or strings. Arrays have a length which is the number of items they contain. You get and change the values of items at different places in an array. You find items in an array by knowing their positions.

Arrays are flexible, they can grow and shrink in size. You can add and remove items at any place in the array.

Create an Array
----------------

An array is created by making a list of items.

.. image:: pictures/data_array1.JPG
   :scale: 100 %

Here the array automatically becomes an array of numbers because it was created with items that are numbers.

You can use different types, like string.

.. image:: pictures/data_array2.JPG
   :scale: 100 %

Items in an Array
------------------

When an item is added to an array is becomes an element of the array. Array elements are found using an index. This is the position in the array of the element, or the element’s order in the array. The positions in an array begin with the index of 0. The first place in the array is 0, the second place is 1, and so on.

.. image:: pictures/data_array3.JPG
   :scale: 100 %

Length of Arrays
-----------------

Arrays always have a length. The last element of an array has an index that is length of array - 1. Indexes begin with 0 so the last element’s index is one less than the array length.

.. image:: pictures/data_array4.JPG
   :scale: 100 %

Advanced
---------

Arrays are a very useful way to collect and organize information. There are more advanced operations and functions you can use on arrays. 

Here’s an example using ``insert At`` to insert a number into the middle of an array.

.. image:: pictures/data_array5.JPG
   :scale: 100 %

Variables
==========

Variables are contain any number or text assigned to it.  If it is a number, you can do operation on it such as addition, subtraction etc.  You can also assign another variable to take the value of a variable.

To make a variable, go to the Variable section of the block selection and select make variable.

.. image:: pictures/variable_make.JPG
   :scale: 100 %

You can assign any name, make sure it describes what is inside the variable, such as temperature, or age.

.. image:: pictures/variable_new.JPG
   :scale: 100 %

To assign (set) a variable’s value:

.. image:: pictures/variable.JPG
   :scale: 100 %

To assign a string to a variable:

.. image:: pictures/variable1.JPG
   :scale: 100 %

To get a variable’s value:

.. image:: pictures/variable2.JPG
   :scale: 100 %

To change a variable’s value:

.. image:: pictures/variable3.JPG
   :scale: 100 %



Challenges
-----------

1.  Generate 20 random numbers and store it in a list, then determine if the number is a prime number or not.

2.  For a give sentence below, check how many characters are there.::

	"Be not afraid of greatness. Some are born great, 
	some achieve greatness, and others have greatness thrust upon them."