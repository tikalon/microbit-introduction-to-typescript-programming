***************
Thermometer
***************

The thermometer on the micro:bit is embedded in one of the chips – and chips get warm when you power them up. Consequently, it doesn’t measure room temperature all that well. The chip that is used to measure temperature can be found on the left hand side of the back of the micro:bit:

.. image:: pictures/thermometer.jpg
   :scale: 120 %


Temperature
================
There is only one basic function for the thermometer – to get the temperature, which is measured in degrees Celsius.  The micro:bit can find the temperature nearby by checking how hot its computer chips are.

.. image:: pictures/temperature_block.JPG
   :scale: 100 %

The micro:bit checks how hot its CPU (main computer chip) is. Because the micro:bit does not usually get very hot, the temperature of the CPU is usually close to the temperature of wherever you are. The micro:bit might warm up a little if you make it work hard, though!

Example 1 - Micro:bit Thermometer
-----------------------------------

The following example uses ``temperature`` and ``show number`` to show the temperature of the room.

.. image:: pictures/temperature_example1.JPG
   :scale: 100 %


Example 2 - Fahrenheit Thermometer
------------------------------------

This program measures the temperature using Fahrenheit degrees. Fahrenheit is a way of measuring temperature that is commonly used in the United States. To make a Celsius temperature into a Fahrenheit one, multiply the Celsius temperature by 18, divide by 10 and add 32.

.. image:: pictures/temperature_example2.JPG
   :scale: 100 %


.. note::

	Try comparing the temperature your micro:bit shows to a real thermometer in the same place. You might be able to figure out how much to subtract from the number the micro:bit shows to get the real temperature. Then you can change your program so the micro:bit is a better thermometer.


Challenge
==========

1. Make the LEDs change pattern as temperature changes



